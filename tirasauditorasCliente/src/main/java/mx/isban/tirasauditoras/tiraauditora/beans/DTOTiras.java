package mx.isban.tirasauditoras.tiraauditora.beans;

import java.io.Serializable;
import java.util.ArrayList;

public class DTOTiras implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 559435902153399212L;
	
	private int impresion;
	private String noOperacion;
	private String hora;
	private String operacion;
	private String usuario;
	private String tipoOperacion;
	private String cuenta;
	private String Folio;
	private String importe;
	private String estado;
	private String pVenta;
	private String traSabado;
	private String descripcion;
	private String codError;
	private String enLinea;
	private String cveDivisa;
	private String cveCustodio2;
	private String arrastre;
	private String observacion;
	private String tipo;
	private String denominacion;
	private String cantidad;
	private String refer_orig;
	private String banco;
	private String plaza;
	private String verifica_firma;
	/**
	 * Clave de custodio/identificador de ATS.
	 */
	private String claveCustodio;
	/**
	 * Origen de la operacion
	 */
	private String origen;
	private ArrayList cheques;
	private ArrayList inv_amon;
	private ArrayList relacionados;

	/**
	*   @return (String)Valor de cuenta
	*/
	public String getCuenta() {
		return cuenta;
	}

	/**
	*   @return (String)Valor del estado
	*/
	public String getEstado() {
		return estado;
	}

	/**
	*   @return (String)Valor de folio
	*/
	public String getFolio() {
		return Folio;
	}

	/**
	*   @return (String)Valor de la hora
	*/
	public String getHora() {
		return hora;
	}

	/**
	*   @return (String)Valor de cuenta
	*/
	public String getImporte() {
		return importe;
	}

	/**
	*   @return (String)Valor de n�mero de operaci�n
	*/
	public String getNoOperacion() {
		return noOperacion;
	}

	/**
	*   @return (String)Valor de operaci�n
	*/
	public String getOperacion() {
		return operacion;
	}

	/**
	*   @return (String)Valor de tipo de operaci�n
	*/
	public String getTipoOperacion() {
		return tipoOperacion;
	}

	/**
	*   @return (String)Valor de usuario
	*/
	public String getUsuario() {
		return usuario;
	}

	/**
	 * @param (String)Valor de la cuenta
	 */
	public void setCuenta(String string) {
		cuenta = string;
	}

	/**
	 * @param (String)Valor del estado
	 */
	public void setEstado(String string) {
		estado = string;
	}

	/**
	 * @param (String)Valor del folio
	 */
	public void setFolio(String string) {
		Folio = string;
	}

	/**
	 * @param (String)Valor de la hora
	 */
	public void setHora(String string) {
		hora = string;
	}

	/**
	 * @param (String)Valor del importe
	 */
	public void setImporte(String string) {
		importe = string;
	}

	/**
	 * @param (String)Valor del n�mero de operaci�n
	 */
	public void setNoOperacion(String string) {
		noOperacion = string;
	}

	/**
	 * @param (String)Valor de la operaci�n
	 */
	public void setOperacion(String string) {
		operacion = string;
	}

	/**
	 * @param (String)Valor del tipo de operaci�n
	 */
	public void setTipoOperacion(String string) {
		tipoOperacion = string;
	}

	/**
	 * @param (String)Valor del usuario
	 */
	public void setUsuario(String string) {
		usuario = string;
	}

	/**
	 * @return (int)Bandera de impresion
	 */
	public int getImpresion() {
		return impresion;
	}

	/**
	 * @param (int)Bandera de impresion
	 */
	public void setImpresion(int i) {
		impresion = i;
	}

	/**
	 * @return (String)Valor de Descripci�n
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @return (String)Valor del Punto de Venta
	 */
	public String getPVenta() {
		return pVenta;
	}

	/**
	 * @return (String)Valor de Disponible Sabado
	 */
	public String getTraSabado() {
		return traSabado;
	}

	/**
	 * @param (String) Valor de la Descripcion
	 */
	public void setDescripcion(String string) {
		descripcion = string;
	}

	/**
	 * @param (String) Valor de Punto de Venta
	 */
	public void setPVenta(String string) {
		pVenta = string;
	}

	/**
	 * @param (String) Valor de Disponible Sabado
	 */
	public void setTraSabado(String string) {
		traSabado = string;
	}

	/**
	 * @return
	 */
	public String getCodError() {
		return codError;
	}

	/**
	 * @param string
	 */
	public void setCodError(String string) {
		codError = string;
	}

	/**
	 * @return
	 */
	public String getEnLinea() {
		return enLinea;
	}

	/**
	 * @param string
	 */
	public void setEnLinea(String string) {
		enLinea = string;
	}

	/**
	 * @return
	 */
	public String getCveDivisa() {
		return cveDivisa;
	}

	/**
	 * @param string
	 */
	public void setCveDivisa(String string) {
		cveDivisa = string;
	}

	/**
	 * @return
	 */
	public String getCveCustodio2() {
		return cveCustodio2;
	}

	/**
	 * @param string
	 */
	public void setCveCustodio2(String string) {
		cveCustodio2 = string;
	}

	/**
	 * @return
	 */
	public String getArrastre() {
		return arrastre;
	}

	/**
	 * @param string
	 */
	public void setArrastre(String string) {
		arrastre = string;
	}

	/**
	 * @return
	 */
	public String getObservacion() {
		return observacion;
	}

	/**
	 * @param string
	 */
	public void setObservacion(String string) {
		observacion = string;
	}

	/**
	 * @return
	 */
	public String getCantidad() {
		return cantidad;
	}

	/**
	 * @return
	 */
	public String getDenominacion() {
		return denominacion;
	}

	/**
	 * @return
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * @param string
	 */
	public void setCantidad(String string) {
		cantidad = string;
	}

	/**
	 * @param string
	 */
	public void setDenominacion(String string) {
		denominacion = string;
	}

	/**
	 * @param string
	 */
	public void setTipo(String string) {
		tipo = string;
	}

	/**
	 * @return
	 */
	public ArrayList getCheques()
	{
		return cheques;
	}

	/**
	 * @return
	 */
	public ArrayList getInv_amon()
	{
		return inv_amon;
	}

	/**
	 * @param list
	 */
	public void setCheques(ArrayList list)
	{
		cheques = list;
	}

	/**
	 * @param list
	 */
	public void setInv_amon(ArrayList list)
	{
		inv_amon = list;
	}

	/**
	 * @return
	 */
	public String getRefer_orig()
	{
		return refer_orig;
	}

	/**
	 * @param string
	 */
	public void setRefer_orig(String string)
	{
		refer_orig = string;
	}

	/**
	 * @return
	 */
	public String getBanco()
	{
		return banco;
	}

	/**
	 * @return
	 */
	public String getPlaza()
	{
		return plaza;
	}

	/**
	 * @return
	 */
	public String getVerifica_firma()
	{
		return verifica_firma;
	}

	/**
	 * Gets the clave custodio.
	 *
	 * @return the clave custodio
	 */
	public String getClaveCustodio() {
		return claveCustodio;
	}

	/**
	 * Sets the clave custodio.
	 *
	 * @param claveCustodio the new clave custodio
	 */
	public void setClaveCustodio(String claveCustodio) {
		this.claveCustodio = claveCustodio;
	}

	/**
	 * Gets the origen.
	 *
	 * @return the origen
	 */
	public String getOrigen() {
		return origen;
	}

	/**
	 * Sets the origen.
	 *
	 * @param origen the new origen
	 */
	public void setOrigen(String origen) {
		this.origen = origen;
	}

	/**
	 * @param string
	 */
	public void setBanco(String string)
	{
		banco = string;
	}

	/**
	 * @param string
	 */
	public void setPlaza(String string)
	{
		plaza = string;
	}

	/**
	 * @param string
	 */
	public void setVerifica_firma(String string)
	{
		verifica_firma = string;
	}

	/**
	 * @return
	 */
	public ArrayList getRelacionados()
	{
		return relacionados;
	}

	/**
	 * @param list
	 */
	public void setRelacionados(ArrayList list)
	{
		relacionados = list;
	}

}
