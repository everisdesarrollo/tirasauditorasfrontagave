package mx.isban.tirasauditoras.tiraauditora.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class LoadProperties {
	/**
	 * Referencia a las propiedades
	 */
	private final static Properties PROPERTIES = new Properties();
	/**
	 * Referencia a la instancia
	 */
	private static final LoadProperties LOAD_PROPERTIES = new  LoadProperties();
	
	/**
	 * Metodo singleton 
	 * @return LoadProperties Objeto del tipo LoadProperties 
	 */
	public static LoadProperties getInstance(){
		return LOAD_PROPERTIES;
	}
	
	/**
	 * Constructor privado
	 */
	private LoadProperties(){
		File file = null;
		InputStream inputPropoties = null;
		Properties rutaProporties = new Properties();
		InputStream input = getClass().getClassLoader().getResourceAsStream("ruta.properties");
		try {
			if(input!=null){
				rutaProporties.load(input);
				String ruta = (String)rutaProporties.get("rutaProperties");
				input.close();
				input = null;
				file = new File(ruta);
				if(file.exists()){
					inputPropoties = new FileInputStream(file);
					PROPERTIES.load(inputPropoties);
					inputPropoties.close();
					inputPropoties = null;
				}else{
					throw new FileNotFoundException("Archvio de propiedades no se encuentra en la ruta"+ruta);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			if(input!=null){
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(inputPropoties!=null){
				try {
					inputPropoties.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public String getValue(String name) {
		String value = null;
		if(PROPERTIES!= null){
			value = PROPERTIES.getProperty(name);
		}
		return value;
	}

}
