package mx.isban.tirasauditoras.samples.ejb;

import mx.isban.tirasauditoras.samples.beans.CicsBean;
import mx.isban.tirasauditoras.samples.beans.ResultSamplesBean;
import mx.isban.tirasauditoras.samples.beans.SamplesBean;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;

/**
 * @author Arquitectura Tecnica
 * Interfaz de servicio de negocio de ejemplos
 */
public interface BOSamples {
	/**
	 * @param param Bean de ejemplo
	 * @param asb Objeto de arquitectura
	 * @return Objeto lleno con resultado
	 */
	public ResultSamplesBean usoIsbanDataAccessDatabase(SamplesBean valor, ArchitechSessionBean asb) 
			throws BusinessException;
	/**
	 * @param param Bean de ejemplo
	 * @param asb Objeto de arquitectura
	 * @return Objeto lleno con resultado
	 */
	public ResultSamplesBean usoIsbanDataAccessMq(SamplesBean valor, ArchitechSessionBean asb) 
			throws BusinessException;
	/**
	 * @param param Bean de ejemplo
	 * @param asb Objeto de arquitectura
	 * @return Objeto lleno con resultado
	 */
	public CicsBean usoIsbanDataAccessCics(CicsBean valor,
			ArchitechSessionBean asb) throws BusinessException;
	/**
	 * @param param Bean de ejemplo
	 * @param asb Objeto de arquitectura
	 * @return Objeto lleno con resultado
	 */
	public ResultSamplesBean usoAuditoriaTrans(SamplesBean valor, ArchitechSessionBean asb) 
			throws BusinessException;
	/**
	 * @param param Bean de ejemplo
	 * @param asb Objeto de arquitectura
	 * @return Objeto lleno con resultado
	 */
	public ResultSamplesBean usoAuditoriaAdmin(SamplesBean valor, ArchitechSessionBean asb) 
			throws BusinessException;
	/**
	 * @param param Bean de ejemplo
	 * @param asb Objeto de arquitectura
	 * @return Objeto lleno con resultado
	 */
	public ResultSamplesBean usoAuditoriaBitacora(SamplesBean valor, ArchitechSessionBean asb)
			throws BusinessException;
	
	/**
	 * @param param Bean de ejemplo
	 * @param asb Objeto de arquitectura
	 * @return Objeto lleno con resultado
	 */
	public ResultSamplesBean usoMensajeria(SamplesBean valor, ArchitechSessionBean asb)
			throws BusinessException;



}
