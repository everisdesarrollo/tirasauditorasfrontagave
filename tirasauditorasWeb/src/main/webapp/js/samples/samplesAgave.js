/**
 * Javascript de modulo de samples. Ejemplo de uso de componente de Configuracion
 */
function doUsoConfig() {
		
		// Elementos necesarios para evitar CSFR
		var token = $("meta[name='_csrf']").attr("content");
		var header = $("meta[name='_csrf_header']").attr("content");
		
		$.ajax({
			type: "POST",
			// Elementos necesarios para evitar CSFR
			beforeSend: function (request)
            {
				if(token){
					request.setRequestHeader(header, token);
				}
            },
			url: "/tirasauditorasWeb/samples/usoConfig.do",
			success: function(response){
				if(response.status == "SUCCESS"){
					alert("Exito!! Por favor, revisa tus logs");
				 }else{
					 alert("Error");
				    
				 }
			 },
			 error: function(e){
				 alert('Error: ' + e);
		 }
	});
}

/**
 * Javascript de modulo de samples. Ejemplo de uso de componente de Logging
 */
function doUsoLogging() {
		
		// Elementos necesarios para evitar CSFR
		var token = $("meta[name='_csrf']").attr("content");
		var header = $("meta[name='_csrf_header']").attr("content");
		
		$.ajax({
			type: "POST",
			// Elementos necesarios para evitar CSFR
			beforeSend: function (request)
            {
				if(token){
					request.setRequestHeader(header, token);
				}
            },
			url: "/tirasauditorasWeb/samples/usoLogging.do",
			success: function(response){
				if(response.status == "SUCCESS"){
					alert("Exito!! Por favor, revisa tus logs");
				 }else{
					 alert("Error");
				    
				 }
			 },
			 error: function(e){
				 alert('Error: ' + e);
		 }
	});
}


/**
 * Javascript de modulo de samples. Ejemplo de uso de componente de IsbanDataAccess Database
 */
function doUsoIsbanDataAccessDatabase() {
		
		// Elementos necesarios para evitar CSFR
		var token = $("meta[name='_csrf']").attr("content");
		var header = $("meta[name='_csrf_header']").attr("content");
		
		$.ajax({
			type: "POST",
			// Elementos necesarios para evitar CSFR
			beforeSend: function (request)
            {
				if(token){
					request.setRequestHeader(header, token);
				}
            },
			url: "/tirasauditorasWeb/samples/usoIsbanDataAccessDatabase.do",
			success: function(response){
				if(response.status == "SUCCESS"){
					alert("Exito!! Por favor, revisa tus logs");
				 }else{
					 alert("Error");
				    
				 }
			 },
			 error: function(e){
				 alert('Error: ' + e);
		 }
	});
}


/**
 * Javascript de modulo de samples. Ejemplo de uso de componente de IsbanDataAccess MQ
 */
function doUsoIsbanDataAccessMq() {
		
		// Elementos necesarios para evitar CSFR
		var token = $("meta[name='_csrf']").attr("content");
		var header = $("meta[name='_csrf_header']").attr("content");
		
		$.ajax({
			type: "POST",
			// Elementos necesarios para evitar CSFR
			beforeSend: function (request)
            {
				if(token){
					request.setRequestHeader(header, token);
				}
            },
			url: "/tirasauditorasWeb/samples/usoIsbanDataAccessMq.do",
			success: function(response){
				if(response.status == "SUCCESS"){
					alert("Exito!! Por favor, revisa tus logs");
				 }else{
					 alert("Error");
				    
				 }
			 },
			 error: function(e){
				 alert('Error: ' + e);
		 }
	});
}

/**
 * Javascript de modulo de samples. Ejemplo de uso de componente de IsbanDataAccess Cics
 */
function doUsoIsbanDataAccessCics() {
		
		// Elementos necesarios para evitar CSFR
		var token = $("meta[name='_csrf']").attr("content");
		var header = $("meta[name='_csrf_header']").attr("content");
		var transaccion = $('#transaccion').val();
		var usuario = $('#usuario').val();
		var longitud = $('#longitud').val();
		var trama = $('#trama').val();
		$("#respuesta").html("");
		
		$.ajax({
			type: "POST",
			// Elementos necesarios para evitar CSFR
			beforeSend: function (request)
            {
				if(token){
					request.setRequestHeader(header, token);
				}
            },
            data: "transaccion=" + transaccion + "&usuario=" + usuario + "&longitud=" + longitud + "&trama=" + trama,
			url: "/tirasauditorasWeb/samples/usoIsbanDataAccessCics.do",
			success: function(response){
				if(response.status == "SUCCESS"){
					$("#respuesta").html(response.respuesta);
					$('#error').hide('slow');
					
				 }else{
					 $('#error').html("");
					 var errorInfo = "";
					 for(var i =0 ; i < response.respuesta.length ; i++){
						errorInfo += "<br>" + (i + 1) +". " + response.respuesta[i];
					 }
					 $('#error').show('slow');
					 $('#error').html("Por favor corrija los siguiente errores: " + errorInfo);
				 
				    
				 }
			 },
			 error: function(e){
				 alert('Error: ' + e);
		 }
	});
}
/**
 * Javascript de modulo de samples. Ejemplo de uso de componente de Mensajeria
 */
function doUsoMensajeria() {
		
		// Elementos necesarios para evitar CSFR
		var token = $("meta[name='_csrf']").attr("content");
		var header = $("meta[name='_csrf_header']").attr("content");
		
		$.ajax({
			type: "POST",
			// Elementos necesarios para evitar CSFR
			beforeSend: function (request)
            {
				if(token){
					request.setRequestHeader(header, token);
				}
            },
			url: "/tirasauditorasWeb/samples/usoMensajeria.do",
			success: function(response){
				if(response.status == "SUCCESS"){
					alert("Exito!! Por favor, revisa tus logs");
				 }else{
					 alert("Error");
				    
				 }
			 },
			 error: function(e){
				 alert('Error: ' + e);
		 }
	});
}

/**
 * Javascript de modulo de samples. Ejemplo de uso de componente de Auditoria
 */
function doUsoAuditoria() {
		
		// Elementos necesarios para evitar CSFR
		var token = $("meta[name='_csrf']").attr("content");
		var header = $("meta[name='_csrf_header']").attr("content");
		
		$.ajax({
			type: "POST",
			// Elementos necesarios para evitar CSFR
			beforeSend: function (request)
            {
				if(token){
					request.setRequestHeader(header, token);
				}
            },
			url: "/tirasauditorasWeb/samples/usoAuditoria.do",
			success: function(response){
				if(response.status == "SUCCESS"){
					alert("Exito!! Por favor, revisa tus logs");
				 }else{
					 alert("Error");
				    
				 }
			 },
			 error: function(e){
				 alert('Error: ' + e);
		 }
	});
}


/**
 * Javascript de modulo de samples. Ejemplo de uso de componente de cache
 */
function doUsoCache() {
		
		// Elementos necesarios para evitar CSFR
		var token = $("meta[name='_csrf']").attr("content");
		var header = $("meta[name='_csrf_header']").attr("content");
		
		$.ajax({
			type: "POST",
			// Elementos necesarios para evitar CSFR
			beforeSend: function (request)
            {
				if(token){
					request.setRequestHeader(header, token);
				}
            },
			url: "/tirasauditorasWeb/samples/usoCache.do",
			success: function(response){
				if(response.status == "SUCCESS"){
					alert("Exito!! Por favor, revisa tus logs");
				 }else{
					 alert("Error");
				    
				 }
			 },
			 error: function(e){
				 alert('Error: ' + e);
		 }
	});
}


function redirectPage(page){
	window.open(page);
	
}