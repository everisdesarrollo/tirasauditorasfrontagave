package mx.isban.tirasauditoras.tiraauditora.dao;

import java.sql.*;

public interface DAOConexion {
		
		public Connection getConexion() throws SQLException;
		public void CloseConnection();
	
}
