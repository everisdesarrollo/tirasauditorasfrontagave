package mx.isban.tirasauditoras.samples.dao.impl;


import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.beans.LoggingBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.DataAccess;
import mx.isban.agave.dataaccess.channels.cics.dto.RequestMessageCicsDTO;
import mx.isban.agave.dataaccess.channels.cics.dto.ResponseMessageCicsDTO;
import mx.isban.agave.dataaccess.factories.jms.ConfigFactoryJMS;
import mx.isban.tirasauditoras.samples.beans.CicsBean;
import mx.isban.tirasauditoras.samples.dao.DAOSamplesCics;

@Stateless
@Local(DAOSamplesCics.class)
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOSamplesCicsImpl extends Architech implements DAOSamplesCics {

	/** Serial version UID de la clase */
	private static final long serialVersionUID = 6711822210144483632L;

	/** ID de canal a acesar a datos */
	private static final String ID_CANAL = "ID_CANAL_CICS";


	/*
	 * (non-Javadoc) 
	 * @see mx.isban.tirasauditoras.principal.dao.DAOSamplesCics#usoIsbanDataAccessCics(mx.isban.tirasauditoras.samples.beans.BeanSamples, mx.isban.agave.commons.beans.ArchitechSessionBean) 
	 */
	@Override
	public CicsBean usoIsbanDataAccessCics(CicsBean valor, ArchitechSessionBean asb) throws ExceptionDataAccess {
		debug("Ejecutando TRX LB08");

        RequestMessageCicsDTO requestDTO = new RequestMessageCicsDTO();

        final StringBuilder mensajeParaTrama = new StringBuilder();
        CicsBean br = null;
        
        mensajeParaTrama.append("    ")
	        .append(valor.getUsuario())
	        .append(valor.getTransaccion())
	        .append(valor.getLongitud())
	        .append("1123451O00N2")
	        .append(valor.getTrama());

        // Llenando informacion general del dto
        requestDTO.setCodeOperation("COD_OPER_TEST_01_ENVIO_CICS");
        requestDTO.setTypeOperation(ConfigFactoryJMS.OPERATION_TYPE_SEND_AND_RECEIVE_MESSAGE);
//        requestDTO.setTimeout(30000); // Opcional, si no se especifica se toma del archivo de
//                                  // configuracion xml
//        requestDTO.setUser("DEIFSNMX"); // Opcional, si no se especifica se toma del archivo de
//                                       // configuracion xml
        requestDTO.setTransaction(valor.getTransaccion());
        requestDTO.setMessage(mensajeParaTrama.toString());

        // Ejecutando operación
        DataAccess ida = DataAccess.getInstance(requestDTO, new LoggingBean());
        ResponseMessageCicsDTO responseDTO = (ResponseMessageCicsDTO) ida.execute(ID_CANAL);

        // Explotando resultados
        debug("Respuesta que regresa el CICS a la transacción solicitada   : " + responseDTO.getResponseMessage());
        debug("Código de error                 : " + responseDTO.getCodeError());
        debug("Mensaje de error                : " + responseDTO.getMessageError());
        debug("Resultado en Json               : " + responseDTO.getResultToJSONString());

        if (!ConfigFactoryJMS.CODE_SUCCESFULLY.equals(responseDTO.getCodeError())) {
			throw new ExceptionDataAccess(responseDTO.getClass().getName(), responseDTO.getCodeError(), responseDTO.getMessageError());
        }
    
        br = new CicsBean();
        br.setRespuesta(responseDTO.getResponseMessage());
        
		return br;
	}
}