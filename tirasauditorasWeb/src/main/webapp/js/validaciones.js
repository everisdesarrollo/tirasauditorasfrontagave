var operaText = new Array(
							'EGRESO',
							'INGRESO',
							'CONSULTA DE SALDOS',
							'ABONO CONTABLE',
							'CARGO CONTABLE',
							'CANCELACION CUENTA CHEQUES',
							'CANCELACION CUENTA AHORRO',
							'CARGO POR INSTRUCCION',
							'CERTFICACION DE CHEQUE',
							'DEPOSITO OI CHEQUES',
							'DEPOSITO EFVO CHEQUES',
							'DEPOSITO EFVO AHORRO',
							'DEPOSITO DE VOUCHERS',
							'DISPOSICION DE TARJETA DE CREDITO',
							'ENVIO DE REMESA A CAJA GENERAL',
							'PAGO DE CHEQUE CERTIFICADO',
							'PAGO DE CHEQUE',
							'RETIRO DE AHORRO',
							'PAGO DE CHEQUE DE CAJA',
							'PAGO DE CHEQUE PARA DEPOSITO',
							'PAGO CHQ CERTIFICADO PARA DEPOSITO',
							'PAGO CHQ DE CAJA PARA DEPOSITO',
							'PAGO CHEQUE PROCAMPO',
							'PAGO TARJETA DE CREDITO EFECTIVO',
							'PAGO TARJETA DE CREDITO SBC',
							'RECEPCION DE REMESA DE CAJA GENERAL',
							'PAGO DE SERVICIOS',
							'TRANFERENCIA DISP/VISTA',
							'TRANFERENCIA VISTA/DISP',
							'VENTA DE CHEQUES DE CAJA EFVO',
							'VENTA DE CHEQUES DE CAJA NC',
							'CAMBIOS COMPRA DE DIVISA / EFECTIVO',
							'CAMBIOS VENTA DE DIVISA / EFECTIVO',
							'CAMBIOS COMPRA DE DIVISA / DEPOSITO CTA',
							'CAMBIOS VENTA DE DIVISA / CARGO CTA',
							'CAMBIOS VENTA DE GIROS',
							'CAMBIOS DOTACION DE CHEQUES DE VIAJERO',
							'CAMBIOS VENTA CHQ VIAJERO / CARGO CTA',
							'CAMBIOS VENTA CHQ VIAJERO / EFECTIVO',
							'CAMBIOS TRANSFERENCIA INTERNACIONAL DOLARES',
							'CAMBIOS TRANSFERENCIA INTERNACIONAL',
							'CAMBIOS TOMA DE REMESAS',
							'COBRO INMEDIATO',
							'PAGO DIRECTO',
							'INGRESO ATS',
							'EGRESO ATS'
);

var operaValue = new Array(
							'1EGA%',
							'1INA%',
							'SDOC%',
							'1ABC%',
							'1CAC%',
							'1CCT%',
							'1CAA%',
							'1CGO%',
							'1CEC%',
							'1DEO%',
							'1DEP%',
							'1DEA%',
							'1DEV%',
							'1DIT%',
							'1ENB%',
							'1PAC%',
							'1PAC%',
							'1REA%',
							'1PAC%',
							'1PAD%',
							'1PAE%',
							'1PAJ%',
							'1PAP%',
							'1PTC%',
							'1PTC%',
							'1REB%',
							'1S%',
							'1TFD%',
							'1TFI%',
							'1VTC%',
							'1VTJ%',
							'1CBI%',
							'1VBI%',
							'1CDV%',
							'1VDV%',
							'1VGI%',
							'1CDO%',
							'1VTC%',
							'1VTC%',
							'1TRD%',
							'1VTR%',
							'1TRE%',
							'COBI%',
							'1OEN%',
							'INATS',
							'EGATS'
);


var operaTipo = [
		'CE',
		'CE',
		'CE',
		'CE',
		'CE',
		'CE',
		'CE',
		'CE',
		'CE',
		'CE',
		'CE',
		'CE',
		'CE',
		'CE',
		'CE',
		'CE',
		'CE',
		'CE',
		'CE',
		'CE',
		'CE',
		'CE',
		'CE',
		'CE',
		'CE',
		'CE',
		'CE',
		'CE',
		'CE',
		'CE',
		'CE',
		'CE',
		'CE',
		'CE',
		'CE',
		'CE',
		'CE',
		'CE',
		'CE',
		'CE',
		'CE',
		'CE',
		'CE',
		'CE',
		'R',
		'R'];


function validaDigito(campo1, campo2){
	var digito = campo2.value.substr(8,9);
	var numcuenta = campo1.value + campo2.value.substr(0,8);
	var pesos = '234567234567';
	var datos = '21' + numcuenta;
	var suma = 0;
	for(var i=0;i<=11;i++){
		var numero = parseInt(datos.substr(i,1));
		suma = suma + (numero * parseInt(pesos.substr(i,1)));
	}
	var numdigito = suma%11;
	if(numdigito==0)
		numdigito = 0;
	else
		if(numdigito==1)
			numdigito = -1;
		else
			numdigito = 11-numdigito;
	if(digito!=numdigito)
		return false;
	return true;
}
function salta(campo){
	if(document.frmConTir.cuenta1.value.length == 2 && valida(campo,valida_cuenta1)){
		document.frmConTir.cuenta2.focus();
		document.frmConTir.cuenta2.select();
	}
}
function setSuc(sucNo){
	var sucTex = document.frmConTir.cboSuc;
	var fecha = document.frmConTir.fecha.value;
	sucTex[0].selected = true;
	if(fecha.length == 10){
		var dtFecha = new Date(fecha.substr(3,2)+'/'+fecha.substr(0,2)+'/'+fecha.substr(6,4));
		if(dtFecha.getDay()==6){
			for(var i=0;i<sucTex.length;i++){
				if(sucTex[i].value.substr(0,4)==sucNo.value && sucTex[i].value.substr(5).toUpperCase()=='S')
					{sucTex[i].selected = true;
					 document.frmConTir.nomsuc.value = sucTex[i].text;
					 setIdsAts(sucNo.value);
					 return true;}
			}
			alert('No labora los Sabados o no se encuentra ese Punto de Venta');
			return false;
		}else{
			for(var i=0;i<sucTex.length;i++){
				if(sucTex[i].value.substr(0,4)==sucNo.value)
					{sucTex[i].selected = true;
					 document.frmConTir.nomsuc.value = sucTex[i].text;
					 setIdsAts(sucNo.value);
					return true;}
			}
			alert('No se encuentra ese Punto de Venta');
			return false;
		}
	}else{
		alert('Se requiere primero la Fecha');
		sucNo.value = '';
		return false;
	}
}
function fselec(val){
	document.frmConTir.cbtoperacion.value = 0;
	document.frmConTir.nooperacion.value = '';
	document.frmConTir.cuenta1.value = '';
	document.frmConTir.cuenta2.value = '';
	document.frmConTir.usuario.value = '';
	document.frmConTir.horaini.value = '';
	document.frmConTir.horafin.value = '';
	document.frmConTir.minutoini.value = '';
	document.frmConTir.minutofin.value = '';
	document.frmConTir.importeini.value = '';
	document.frmConTir.importefin.value = '';
	document.frmConTir.cbtoperacion.disabled = true;
	document.frmConTir.nooperacion.disabled = true;
	document.frmConTir.cuenta1.disabled = true;
	document.frmConTir.cuenta2.disabled = true;
	document.frmConTir.usuario.disabled = true;
	document.frmConTir.horaini.disabled = true;
	document.frmConTir.horafin.disabled = true;
	document.frmConTir.minutoini.disabled = true;
	document.frmConTir.minutofin.disabled = true;
	document.frmConTir.importeini.disabled = true;
	document.frmConTir.importefin.disabled = true;
	document.frmConTir.cboIdAts.disabled = true;

	switch(val.value){
		case '1': document.frmConTir.cbtoperacion.disabled = false; break;
		case '2': document.frmConTir.nooperacion.disabled = false; break;
		case '3':
			document.frmConTir.cuenta1.disabled = false;
			document.frmConTir.cuenta2.disabled = false;
		break;
		case '4': document.frmConTir.usuario.disabled = false; break;
		case '5':
			document.frmConTir.horaini.disabled = false;
			document.frmConTir.horafin.disabled = false;
			document.frmConTir.minutoini.disabled = false;
			document.frmConTir.minutofin.disabled = false;
		break;
		case '6':
			document.frmConTir.importeini.disabled = false;
			document.frmConTir.importefin.disabled = false;
		break;
		case '7':
			document.frmConTir.cboIdAts.disabled = false;
		break;
	}
}

function assertField(aField, aFieldProperties) {
    var lMessage = lblCampo+" "+aFieldProperties[1]+": \n";
    var error=false;
    // Check Type
    aField.value = aField.value.trim().deleteQuotes();

    // Check Required Max Length
    if ((aFieldProperties[3]==true || aField.value.length>0) && aFieldProperties[4]>0) {
        if (aField.value.length != aFieldProperties[4]) {
        	for(var i=0;aField.value.length<aFieldProperties[4];i++){
        		aField.value = "0"+aField.value;
        	}
        }
    }

    // Valor entero
    if (aFieldProperties[2] == "N") {
    	var lRegularExpresion=/^\d+$/
        if (!lRegularExpresion.test(aField.value)) {
            lMessage += lblRequiereNumero+"\n";
            error=true;
        } else {
            var intVal = 0;
            intVal = parseInt(aField.value);
            if (intVal < 0) {
                lMessage += lblRequiereNumeroPositivo+"\n";
                error=true;
            }
    	}
    }
    // Valor entero mayor a cero
    if (aFieldProperties[2] == "N+") {
    	var lRegularExpresion=/^\d+$/
        if (!lRegularExpresion.test(aField.value)) {
            lMessage += lblRequiereNumero+"\n";
            error=true;
        } else {
            var intVal = 0;
            intVal = parseInt(aField.value);
            if (intVal <= 0) {
                lMessage += lblRequiereNumeroMayor0+"\n";
                error=true;
            }
    	}
    }
    // Valor decimal
    if (aFieldProperties[2] == "F+") {
        if (isNaN(aField.value) || aField.value.length<=0) {
            lMessage += lblRequiereNumero+"\n";
            error=true;
        }else{
        	var intVal = 0;
            intVal = parseFloat(aField.value);
            if (intVal < 0) {
                lMessage += lblRequiereNumeroMayor0+"\n";
                error=true;
            }else{
		        aField.value = "" + aField.value //convert value to string
		        precision = parseInt('3');
		        var whole = "" + Math.round(aField.value * Math.pow(10, precision));
		        var decPoint = whole.length - precision;
		        if(decPoint != 0){
		        	if(aField.value.substring(0,3)=='0.0' || aField.value.substring(0,2)=='.0'){
		            	aField.value = "0.0";
		            	aField.value += whole.substring(decPoint, whole.length-1);
		        	}else{
		        			aField.value = whole.substring(0, decPoint);
		            	aField.value += ".";
		            	aField.value += whole.substring(decPoint, whole.length-1);
		        			if(aField.value == '.')
		        				aField.value = '0.0';
		        		}
		        }else{
		            aField.value = 0;
		            aField.value += ".";
		            aField.value += whole.substring(decPoint, whole.length-1);
		        }
			}
		}
    }
    if (aFieldProperties[2] == "A") {
    	aField.value = aField.value.toUpperCase();
    }
    if (aFieldProperties[2] == "@") {
    var lRegularExpresion=/\.*@.*\..*/i
    result=lRegularExpresion.exec(aField.value);
        if (result!=null) {
        lMessage+=lblInvalidMailFormat+": ej: nombre@empresa.com";
        }
    }
    if (aFieldProperties[2] == "D") {
	result=checkDateOrNull(aFieldProperties[4], aFieldProperties[5], aField)
        if (!result) {
        lMessage+=lblInvalidDateFormat;
	error=true;
        }
    }
    // Valor hora
    if (aFieldProperties[2] == "H") {
    	var lRegularExpresion=/^\d+$/
        if (!lRegularExpresion.test(aField.value)) {
            lMessage += lblRequiereNumero+"\n";
            error=true;
        } else {
            var intVal = 0;
            intVal = new Number(aField.value);
            if (intVal < 0 || intVal > 23) {
                lMessage += lblRequiereRangodeHoras+"\n";
                error=true;
            }
    	}
    }
    // Valor minutos
    if (aFieldProperties[2] == "M") {
    	var lRegularExpresion=/^\d+$/
        if (!lRegularExpresion.test(aField.value)) {
            lMessage += lblRequiereNumero+"\n";
            error=true;
        } else {
            var intVal = 0;
            intVal = new Number(aField.value);
            if (intVal < 0 || intVal > 59) {
                lMessage += lblRequiereRangodeMinutos+"\n";
                error=true;
            }
    	}
    }


    // Check Required
    if (aFieldProperties[3] == true) {
        if (aField.value.length == 0) {
            lMessage += lblRequerido+"\n";
            error=true;
        }
    }

    // Check Top Max Length
    if ((aFieldProperties[3]==true || aField.value.length>0) && aFieldProperties[5]>0) {
        if (aField.value.length > aFieldProperties[5]) {
            lMessage+=lblLongitudPermitida+" "+aFieldProperties[5]+" "+lblCaracteres+"\n";
            error=true;
        }
    }

    if (error) {
        return lMessage;
    }
    return "";
}
function prototype_leftTrim() {
    return this.replace(/^\s+/, "");
}

function prototype_rightTrim() {
    return this.replace(/\s+$/, "");
}

function prototype_trim() {
    return this.leftTrim().rightTrim();
}

function prototype_deleteSimpleQuotes() {
    return this.replace(/\'/g, "");
}

function prototype_deleteDoubleQuotes() {
    return this.replace(/\"/g, "");
}

function prototype_deleteQuotes() {
    return this.deleteSimpleQuotes().deleteDoubleQuotes();
}

function prototype_deleteLeftZeroes() {
    return this.replace(/^0+/, "");
}

function prototype_tokenize(ch) {
    var array = new Array();
    if (this == '') return array;
    var s = this;
    var count = 0;
    while (s.indexOf(ch) != -1) {
        array[count] = s.substring(0, s.indexOf(ch));
        count ++;
        s = s.substring(s.indexOf(ch) + 1);
    }
    array[count] = s;
    return array;
}

function prototype_matchPattern(pattern) {
    pat = "^";
    for (var i = 0; i < pattern.length; i ++) {
        if (pattern.charAt(i) == 'n') pat += "\\d";
        else if (pattern.charAt(i) == 'x') pat += "\\w";
        else if (pattern.charAt(i) == 'l') pat += "[a-z]";
        else if (pattern.charAt(i) == 'L') pat += "[A-Z]";
        else if (pattern.charAt(i) == ' ') pat += "\\s";
        else if (pattern.charAt(i) == 'S') pat += "\\s*";
        else pat += pattern.charAt(i);
    }
    pat += "$";
    var RE = new RegExp(pat);
    return RE.test(this);
}

function prototype_matchDatePattern(pattern, arr) {
    var pat = pattern.replace(/[a-z]|[A-Z]/g, "n");
    if (! this.matchPattern(pat)) return false;
    var array = this.replace(/-/g, "/").tokenize("/");
    var s = pattern.replace(/-/g, "/").tokenize("/");
    var day = array[getIndexContaining(s, "d")];
    var month = array[getIndexContaining(s, "m")];
    var year = array[getIndexContaining(s, "a")];
    if (! isValidDay(day, month, year)) return false;
    if (! isValidMonth(month)) return false;
    if (! isValidYear(year)) return false;
    arr[arr.length] = getDate(day, month, year);
    return true;
}

function prototype_matchNewDatePattern(pattern, arr) {
    var pat = pattern.replace(/[a-z]|[A-Z]/g, "n");
    if (! this.matchPattern(pat)) return -1;
    var array = this.replace(/-/g, "/").tokenize("/");
    var s = pattern.replace(/-/g, "/").tokenize("/");
    var day = array[getIndexContaining(s, "d")];
    var month = array[getIndexContaining(s, "m")];
    var year = array[getIndexContaining(s, "a")];
    if (! isValidDay(day, month, year)) return -2;
    if (! isValidMonth(month)) return -3;
    if (! isValidYear(year)) return -4;
    arr[arr.length] = getDate(day, month, year);
    return 0;
}

function prototype_startsWith(str) {
    if (this.indexOf(str) == 0) return true;
    return false;
}

function prototype_endsWith(str) {
    var index = this.lastIndexOf(str);
    if (index == -1) return false;
    if (this.length - str.length == index) return true;
    return false;
}

String.prototype.leftTrim = prototype_leftTrim
String.prototype.rightTrim = prototype_rightTrim
String.prototype.trim = prototype_trim
String.prototype.deleteSimpleQuotes = prototype_deleteSimpleQuotes
String.prototype.deleteDoubleQuotes = prototype_deleteDoubleQuotes
String.prototype.deleteQuotes = prototype_deleteQuotes
String.prototype.deleteLeftZeroes = prototype_deleteLeftZeroes
String.prototype.tokenize = prototype_tokenize
String.prototype.matchPattern = prototype_matchPattern
String.prototype.matchDatePattern = prototype_matchDatePattern
String.prototype.matchNewDatePattern = prototype_matchNewDatePattern
String.prototype.startsWith = prototype_startsWith
String.prototype.endsWith = prototype_endsWith


function cambiaformato(pFecha){
	var DFecha1 = pFecha.substring(0,pFecha.indexOf('/'));
	var MFecha1 = pFecha.substring((pFecha.indexOf('/')+1),pFecha.lastIndexOf('/'));
	var YFecha1 = pFecha.substring(pFecha.lastIndexOf('/')+1,pFecha.length);
	return MFecha1+'/'+DFecha1+'/'+YFecha1;
}

function checkDateOrNull(dateFormat, dateSeparator, field)
{
// Checks for the following valid date formats:
// MM/DD/YY   MM/DD/YYYY   MM-DD-YY   MM-DD-YYYY
// Also separates date into month, day, and year variables

	if(field.value.length==0)return true;

	var dateRegExp;
	var matchArray;

	var day;
	var month;
	var year;

	var currentDay;
	var currentMonth;
	var currentYear;

	var now;

	currentDay = thisDay();
	currentMonth = thisMonth();
	currentYear = thisYear();

	switch (dateFormat)
	{
		case 0 :
			now = currentDay.toString() + dateSeparator + currentMonth.toString() + dateSeparator + currentYear.toString();
			break;
		case 1 :
			now = currentDay.toString() + dateSeparator + currentYear.toString() + dateSeparator + currentMonth.toString();
			break;
		case 2 :
			now = currentMonth.toString() + dateSeparator + currentYear.toString() + dateSeparator + currentDay.toString();
			break;
		case 3 :
			now = currentMonth.toString() + dateSeparator + currentDay.toString() + dateSeparator + currentYear.toString();
			break;
		case 4 :
			now = currentYear.toString() + dateSeparator + currentMonth.toString() + dateSeparator + currentDay.toString();
			break;
		case 5 :
			now = currentYear.toString() + dateSeparator + currentDay.toString() + dateSeparator + currentMonth.toString();
			break;
	}

	switch (dateFormat)
	{
		case 0 :
			dateRegExp = /^(\d{1}|\d{2})(\/|-|.)(\d{1}|\d{2})\2(\d{4})$/;	// dd-mm-yyyy
			break;
		case 1 :
			dateRegExp = /^(\d{1}|\d{2})(\/|-|.)(\d{4})\2(\d{1}|\d{2})$/;	// dd-yyyy-mm
			break;
		case 2 :
			dateRegExp = /^(\d{1}|\d{2})(\/|-|.)(\d{4})\2(\d{1}|\d{2})$/;	// mm-yyyy-dd
			break;
		case 3 :
			dateRegExp = /^(\d{1}|\d{2})(\/|-|.)(\d{1}|\d{2})\2(\d{4})$/;	// mm-dd-yyyy
			break;
		case 4 :
			dateRegExp = /^(\d{4})(\/|-|.)(\d{1}|\d{2})\2(\d{1}|\d{2})$/;	// yyyy-mm-dd
			break;
		case 5 :
			dateRegExp = /^(\d{4})(\/|-|.)(\d{1}|\d{2})\2(\d{1}|\d{2})$/;	// yyyy-dd-mm
			break;
	}

	var matchArray = field.value.match(dateRegExp); // is the format ok?
	if (matchArray == null)
	{
		//alert("Fecha con formato invalido")
		//field.value = now;
		return false;
	}

	switch (dateFormat)
	{
		case 0 :
			day = matchArray[1];	// dd-mm-yyyy
			separator = matchArray[2];
			month = matchArray[3];
			year = matchArray[4];
			break;
		case 1 :
			day = matchArray[1];	// dd-yyyy-mm
			separator = matchArray[2];
			year = matchArray[3];
			month = matchArray[4];
			break;
		case 2 :
			month = matchArray[1];	// mm-yyyy-dd
			separator = matchArray[2];
			year = matchArray[3];
			day = matchArray[4];
			break;
		case 3 :
			month = matchArray[1];	// mm-dd-yyyy
			separator = matchArray[2];
			day = matchArray[3];
			year = matchArray[4];
			break;
		case 4 :
			year = matchArray[1];	// yyyy-mm-dd
			separator = matchArray[2];
			month = matchArray[3];
			day = matchArray[4];
			break;
		case 5 :
			year = matchArray[1];	// yyyy-dd-mm
			separator = matchArray[2];
			day = matchArray[3];
			month = matchArray[4];
			break;
	}

	switch (dateFormat)
	{
		case 0 :
			now = currentDay.toString() + separator.toString() + currentMonth.toString() + separator.toString() + currentYear.toString();
			break;
		case 1 :
			now = currentDay.toString() + separator.toString() + currentYear.toString() + separator.toString() + currentMonth.toString();
			break;
		case 2 :
			now = currentMonth.toString() + separator.toString() + currentYear.toString() + separator.toString() + currentDay.toString();
			break;
		case 3 :
			now = currentMonth.toString() + separator.toString() + currentDay.toString() + separator.toString() + currentYear.toString();
			break;
		case 4 :
			now = currentYear.toString() + separator.toString() + currentMonth.toString() + separator.toString() + currentDay.toString();
			break;
		case 5 :
			now = currentYear.toString() + separator.toString() + currentDay.toString() + separator.toString() + currentMonth.toString();
			break;
	}

	if (month < 1 || month > 12)
	{ // check month range
		alert("El mes debe estar entre 1 y 12.");
		//field.value = now;
		return false;
	}

	if (day < 1 || day > 31)
	{
		alert("El dia debe estar entre 1 y 31.");
		//field.value = now;
		return false;
	}

	if ((month==4 || month==6 || month==9 || month==11) && day==31)
	{
		alert("El mes de "+month+" no tiene 31 dias !")
		//field.value = now;
		return false
	}

	if (month == 2)
	{ // check for february 29th
		var isleap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
		if (day>29 || (day==29 && !isleap))
		{
			alert("Febrero de " + year + " no tiene " + day + " dias !");
			//field.value = now;
			return false;
		}
	}
	return true;   // date is valid

}

var lblCampo = 'El campo';
var lblLongitudPermitida='Longitud permitida';
var lblLongitudRequerida='Longitud requerida';
var lblRequerido="es requerido";
var lblRequiereNumero='requiere un valor num\u00E9rico';
var lblRequiereNumeroPositivo='requiere un valor num\u00E9rico positivo ';
var lblRequiereNumeroMayor0='requiere un valor num\u00E9rico mayor a cero ';
var lblCaracteres='caracteres';
var lblCaracteresPermitidos='Caracteres permitidos';
var lblInvalidMailFormat='El formato de email es inv\u00E1lido';
var lblInvalidDateFormat='El formato de la fecha es inv\u00E1lido';
var lblRequiereRangodeHoras='El formato de las horas es de 00 a 23';
var lblRequiereRangodeMinutos='El formato de los minutos es de 00 a 59';

function thisDay()
{

	var today = new Date();
	return today.getDate();
}

function thisMonth()
{

	var today = new Date();
	return today.getMonth() + 1;
}

function thisYear()
{

	var today = new Date();
	var year;

	year = today.getYear();

	if (parseInt(navigator.appVersion) >= 4)
	{
		if	(navigator.appName == "Netscape" )
		{
			year = 2000 + year - 100;
		}
	}

	return year;
}

function thisHours(timeFormat)
{

	var today = new Date();
	var hours = today.getHours();

	if ( timeFormat == 0 )
		if ( hours > 12 )
			hours = hours - 12;

	return hours.toString();

}

function thisMinutes()
{

	var today = new Date();
	var minutes = today.getMinutes();

	if (minutes < 10 )
		return "0" + minutes.toString();
	else
		return minutes.toString();

}

function thisSeconds()
{

	var today = new Date();
	var seconds = today.getSeconds();

	if (seconds < 10 )
		return "0" + seconds.toString();
	else
		return seconds.toString();

}

function thisMeridian(timeFormat)
{

	var today = new Date();
	var hours = today.getHours();
	var meridian;

	if ( timeFormat == 0 )
		if ( hours > 12 )
			meridian = "PM";
		else
			meridian = "AM";
	else
		meridian = "";

	return meridian;

}
function setIdsAts(sucNo){
	var xmlhttp;
	if (window.XMLHttpRequest){
		// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	}else{
		// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}

	xmlhttp.onreadystatechange=function(){
	      if (xmlhttp.readyState===4 && xmlhttp.status===200)
	      {
		      if (window.DOMParser){
		       	  parser=new DOMParser();
		    	  xmlDoc=parser.parseFromString(xmlhttp.responseText,"text/xml");
		      }else{
		    	  // Internet Explorer
		    	  xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
		    	  xmlDoc.async=false;
		    	  xmlDoc.loadXML(xmlhttp.responseText);
		      }
		      var item=xmlDoc.getElementsByTagName("id");

		      var select=document.getElementById("cboIdAts");
		      select.innerHTML = "";

		      if(item.length>0 ){
			      for(var i=0;i<item.length;i++) {
			    	   var id=item[i].childNodes[0].nodeValue;
			    	   addItem("cboIdAts", id, id);
			      }
		      }else{
		    	  addItem("cboIdAts", "No existen ids de ATS para este punto de venta", "");
		      }
	      }
	};

	xmlhttp.open("GET","../servlet/TiraServlet?accion=ajax&operacion=getIdsAts&idSuc="+sucNo,true);
	xmlhttp.send();

}

function addItem(idCombo, text, value)
{
    // Create an Option object
    var opt = document.createElement("option");

    // Add an Option object to Drop Down/List Bo
    document.getElementById(idCombo).options.add(opt);

    // Assign text and value to Option object
    opt.text = text;
    opt.value = value;

}

function tipoOrigen(tipo){

	document.getElementById("esIdAts").disabled = false;

	var select = document.getElementById("cbtoperacion");

	if(tipo==="CE"){
		document.getElementById("esNinguno").checked = true;
		document.getElementById("esIdAts").disabled = true;
		document.getElementById("cboIdAts").disabled = true;

		select.innerHTML = "";

		addItem("cbtoperacion", "Seleccione tipo...", "0");
		for(var i=0;i<operaText.length;i++)
		{
			if(operaTipo[i]==="CE"){
				addItem("cbtoperacion", operaText[i], operaValue[i]);
			}
		}

	}else if(tipo==="R"){

		select.innerHTML = "";

		addItem("cbtoperacion", "Seleccione tipo...", "0");
		for(var j=0;j<operaText.length;j++)
		{
			if(operaTipo[j]==="R"){
				addItem("cbtoperacion", operaText[j], operaValue[j]);
			}
		}

	}else{

		select.innerHTML = "";

		addItem("cbtoperacion", "Seleccione tipo...", "0");
		for(var k=0;k<operaText.length;k++)
		{
			addItem("cbtoperacion", operaText[k], operaValue[k]);
		}

	}
}