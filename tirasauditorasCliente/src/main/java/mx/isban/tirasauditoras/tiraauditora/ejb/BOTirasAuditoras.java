package mx.isban.tirasauditoras.tiraauditora.ejb;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

import com.itextpdf.text.pdf.PdfPTable;
import mx.isban.tirasauditoras.tiraauditora.beans.DTOTiras;

public interface BOTirasAuditoras {
	
	//public void finalize();
	public ArrayList consultaTira(String fecha, String pventa, String origen);
	public ArrayList consultaTiraTipo(String fecha, String pventa, String toperacion, String origen);
	public ArrayList consultaTiraOperacion(String fecha, String pventa, String nooperacion, String origen);
	public ArrayList consultaTiraCuenta(String fecha, String pventa, String cuenta, String origen);
	public ArrayList consultaTiraUsuario(String fecha, String pventa, String usuario, String origen);
	public ArrayList consultaTiraHora(String fecha, String pventa, String horaini, String horafin, String origen);
	public ArrayList consultaTiraImporte(String fecha, String pventa, String importeini, String importefin, String origen);
	public ArrayList consultaTiraIdAts(String fecha, String pventa, String idAts, String origen);
	void origenOperacion(DTOTiras tira, String tipoOperacion);
	public ByteArrayOutputStream makePdf(int seleccion, ArrayList datos, String dato1, String dato2, String encabeza, String origen);
	PdfPTable creaEncabezado(int seleccion, String dato1, String dato2, String encabeza, String origen);
	public ArrayList esContingente(String fecha, String pventa);
	public ArrayList allSucursal();
	public String descCustodio(String cve_custodio);
	ArrayList consultaCheques(String fecha, String pventa);
	ArrayList registrosCheques(ArrayList cheques, String referencia);
	ArrayList registrosInv_Amon(ArrayList inv_amon, String referencia);
	ArrayList consultaInv_Amon(String fecha, String pventa);
	String estadoDescripcion(String clave);
	String operacion(DTOTiras val);
	String desc_Servicios(String servicio);
	String contable_Desc(String servicio);
	String rastreo_DSaldos(String trama, String cuenta);
	String descrOprCam(String trama, String oper);
	String parsea(String trama, int pos, int num);
	public ArrayList consultaClaveCustodio(String pventa);
	
}