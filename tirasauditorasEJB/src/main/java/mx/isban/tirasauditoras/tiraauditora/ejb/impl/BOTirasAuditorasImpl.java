package mx.isban.tirasauditoras.tiraauditora.ejb.impl;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import com.itextpdf.text.*;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.tirasauditoras.tiraauditora.beans.DTOTiras;
import mx.isban.tirasauditoras.tiraauditora.dao.impl.DAOConexionImpl;
import mx.isban.tirasauditoras.tiraauditora.ejb.BOTirasAuditoras;
import mx.isban.tirasauditoras.tiraauditora.utilities.LoadProperties;

@Stateless
@Remote(BOTirasAuditoras.class)
@TransactionManagement(TransactionManagementType.BEAN)
public class BOTirasAuditorasImpl extends Architech implements BOTirasAuditoras {

	/** La Constante INGRESO_1ATS contiene la clave de operacion de ingreso de 1ATS. */
	private final static String INGRESO_1ATS = "1IATS";

	/** La Constante EGRESO_1ATS contiene la clave de operacion de egreso de 1ATS. */
	private final static String EGRESO_1ATS = "1EATS";

	/** La Constante INGRESO_ATS contiene la clave de operacion de ingreso de ATS. */
	private final static String INGRESO_ATS = "INATS";

	/** La Constante EGRESO_ATS contiene la clave de operacion de egreso de ATS. */
	private final static String EGRESO_ATS = "EGATS";

	/** La Constante INGRESO_CAJ contiene la clave de operacion de ingreso de cajonera. */
	private final static String INGRESO_CAJ = "1INAM";

	/** La Constante EGRESO_CAJ contiene la clave de operacion de egreso de cajonera. */
	private final static String EGRESO_CAJ = "1EGAM";

	/** La Constante IDENTIFICA_CE origen Cajonera. */
	private final static String IDENTIFICA_CE = "CE";

	/** La Constante IDENTIFICA_R origen Recicladora. */
	private final static String IDENTIFICA_R = "R";

	/** La Constante IDENTIFICA_V origen Ninguno. */
	private final static String IDENTIFICA_V = "";

	/** La Constante CVE_OPERACION contiene el valor para el campo CVE_OPERACION. */
	private final static String CVE_OPERACION = "CVE_OPERACION";

	/** La Constante CVE_CUSTODIO contiene el valor para el campo CVE_CUSTODIO. */
	private final static String CVE_CUSTODIO = "CVE_CUSTODIO";

	/** La Constante COD_ERROR contiene el valor para el campo COD_ERROR. */
	private final static String COD_ERROR = "COD_ERROR";

	/** La Constante EN_LINEA contiene el valor para el campo EN_LINEA. */
	private final static String EN_LINEA = "EN_LINEA";

	/** La Constante CVE_DIVISA contiene el valor para el campo CVE_DIVISA. */
	private final static String CVE_DIVISA = "CVE_DIVISA";

	/** La Constante CVE_CUSTODIO2 contiene el valor para el campo CVE_CUSTODIO2. */
	private final static String CVE_CUSTODIO2 = "CVE_CUSTODIO2";

	/** La Constante DESCRIPCION contiene el valor para el campo DESCRIPCION. */
	private final static String DESCRIPCION = "DESCRIPCION";

	/** La Constante ARRASTRE_EFVO contiene el valor para el campo ARRASTRE_EFVO. */
	private final static String ARRASTRE_EFVO = "ARRASTRE_EFVO";

	/** La Constante OBSERVACION contiene el valor para el campo OBSERVACION. */
	private final static String OBSERVACION = "OBSERVACION";

	/** La variable conexion. */
	DAOConexionImpl conexion= new DAOConexionImpl();

	/** La variable rb. */
	LoadProperties rb = LoadProperties.getInstance();

	/** La variable con. */
	private Connection con;


/**
 * El m�todo TiraClase() es el constructor de la clase abre conexion.
 */
	public BOTirasAuditorasImpl()
	{
		if(con == null)
		{
			try
			{
			con = conexion.getConexion();
			}
			catch (SQLException e) {
				System.out.println("Error in" + this.getClass().getName() + " " +e.getMessage());
			}
		}
	}

/**
 * El m�todo finalize() es el destructor de la clase, cierra conexion.
 *
 * @throws Throwable en caso de que suceda una throwable
 */
	protected void finalize() throws Throwable {
		con.close();
		conexion.CloseConnection();
		super.finalize();
	}

/**
 * El m�todo consultaTira(String, String) obtiene la consulta de la Tira
 * en base al punto de venta y fecha.
 *
 * @param fecha  Fecha de consulta
 * @param pventa Punto de venta
 * @param origen el parametro origen
 * @return  ArrayList valor de respuesta de la consulta
 */
	@Override
	public ArrayList consultaTira(String fecha, String pventa, String origen)
	{
		System.out.println("Prueba ");
		System.out.println("Metodo consultaTira");
		ArrayList resultado = new ArrayList();
		ArrayList resInterno = new ArrayList();
		StringBuffer lsb_query = new StringBuffer();
		PreparedStatement query = null;
		ResultSet rs = null;

		lsb_query = lsb_query.delete(0, lsb_query.length());
		lsb_query.append("SELECT D.REFERENCIA, TO_CHAR(D.FECHA_DIA,'HH24:MI') FECHA_DIA, NVL(D.CVE_OPERACION,'') CVE_OPERACION, CO.DESCRIPCION, D.CVE_CUSTODIO, ");
		lsb_query.append("D.COD_ERROR, D.EN_LINEA, D.CVE_DIVISA, D.IMPORTE, D.ESTADO, D.CVE_CUSTODIO2, D.ARRASTRE_EFVO, D.OBSERVACION, D.REFER_ORIG ");
		lsb_query.append("FROM TA_DIARIO D, TA_COMU_OPERACIONES CO ");
		lsb_query.append("WHERE D.CVE_OPERACION = CO.CVE_OPERACION(+) ");
		if(IDENTIFICA_R.equals(origen)){
		lsb_query.append("AND REFER_ORIG IN (SELECT REFER_ORIG FROM TA_DIARIO TD WHERE TD.CVE_OPERACION IN ('").append(INGRESO_ATS).append("','").append(EGRESO_ATS).append("','").append(INGRESO_1ATS).append("','").append(EGRESO_1ATS).append("') AND TD.CVE_PTOVTA = '").append(pventa).append("' AND TO_CHAR(TD.FECHA_DIA,'DD/MM/YYYY') = '").append(fecha).append("') ");
		//lsb_query.append("AND REFER_ORIG IN (SELECT REFER_ORIG FROM TA_DIARIO TD WHERE TD.CVE_OPERACION IN ('").append(INGRESO_ATS).append("','").append(EGRESO_ATS).append("') AND TD.CVE_PTOVTA = '").append(pventa).append("' AND TO_CHAR(TD.FECHA_DIA,'DD/MM/YYYY') = '").append(fecha).append("') ");
		}
		else if((IDENTIFICA_CE.equals(origen))){
		lsb_query.append("AND REFER_ORIG NOT IN (SELECT REFER_ORIG FROM TA_DIARIO TD WHERE TD.CVE_OPERACION IN ('").append(INGRESO_ATS).append("','").append(EGRESO_ATS).append("','").append(INGRESO_1ATS).append("','").append(EGRESO_1ATS).append("') AND TD.CVE_PTOVTA = '").append(pventa).append("' AND TO_CHAR(TD.FECHA_DIA,'DD/MM/YYYY') = '").append(fecha).append("') ");
		//lsb_query.append("AND REFER_ORIG NOT IN (SELECT REFER_ORIG FROM TA_DIARIO TD WHERE TD.CVE_OPERACION IN ('").append(INGRESO_ATS).append("','").append(EGRESO_ATS).append("') AND TD.CVE_PTOVTA = '").append(pventa).append("' AND TO_CHAR(TD.FECHA_DIA,'DD/MM/YYYY') = '").append(fecha).append("') ");

		}
		lsb_query.append("AND D.CVE_PTOVTA = '").append(pventa).append("' ");
		lsb_query.append("AND TO_CHAR(D.FECHA_DIA,'DD/MM/YYYY') = '").append(fecha).append("' ");
		lsb_query.append("ORDER BY D.REFER_ORIG, D.REFERENCIA  ");
		System.out.println("lsb_query: "+lsb_query.toString());
		int j = 0;
		try
		{
			if(con == null || con.isClosed()){
				con = conexion.getConexion();
			}
			query = con.prepareStatement(lsb_query.toString());
			rs=query.executeQuery();
			while(rs.next())
			{
				DTOTiras valores = new DTOTiras();

				origenOperacion(valores, rs.getString(CVE_OPERACION).trim());

				valores.setNoOperacion(rs.getString("REFERENCIA"));
				valores.setHora(rs.getString("FECHA_DIA"));
				valores.setTipoOperacion(rs.getString(CVE_OPERACION)!=null?rs.getString(CVE_OPERACION):"");
				valores.setUsuario(rs.getString(CVE_CUSTODIO));
				valores.setCodError(rs.getString(COD_ERROR));
				valores.setEnLinea(rs.getString(EN_LINEA));
				valores.setCveDivisa(rs.getString(CVE_DIVISA));
				valores.setImporte(rs.getString("IMPORTE"));
				valores.setEstado(rs.getString("ESTADO"));
				valores.setCveCustodio2(rs.getString(CVE_CUSTODIO2));
				if(rs.getString(DESCRIPCION)!=null && rs.getString(DESCRIPCION).length()>0){
					valores.setOperacion(rs.getString(DESCRIPCION));
				}
				else {
					valores.setOperacion(valores.getTipoOperacion());
				}
				valores.setArrastre(rs.getString(ARRASTRE_EFVO));
				valores.setObservacion(rs.getString(OBSERVACION));
				valores.setRefer_orig(rs.getString("REFER_ORIG"));
				resInterno.add(valores);
				j++;
				if(j==Integer.parseInt(rb.getValue("ta.noregistros")))
				{
					resultado.add(resInterno);
					resInterno = new ArrayList();
					j=0;
				}
			}
			if(j!=0) {
				resultado.add(resInterno);
			}
			if(resultado.size()<=0) {
				resultado = esContingente(fecha, pventa);
			}
			else {
				ArrayList cheques = consultaCheques(fecha,pventa);
				ArrayList inv_amon = consultaInv_Amon(fecha,pventa);
				int tam1 = resultado.size();
				for(int x=0;x< tam1 ;x++) {
					ArrayList pag = (ArrayList)resultado.get(x);
					int tam2 = pag.size();
					for(int y=0;y< tam2; y++) {
						DTOTiras val = (DTOTiras)pag.get(y);
						val.setCheques(registrosCheques(cheques,val.getNoOperacion()));
						val.setInv_amon(registrosInv_Amon(inv_amon,val.getNoOperacion()));
						val.setOperacion(operacion(val));
						val.setCheques(null);
						val.setInv_amon(null);
					}
				}
				cheques = null;
				inv_amon = null;
			}
		}
		catch (SQLException e1) {
			System.out.println("Error in " + getClass().getName() + "\n" + e1);
		}
		finally {
			try {
				if(rs!=null) {
					rs.close();

				}
				if(query!=null) {
					query.close();
				}
				//27/06/2008 Se realiza el cierre de conexion a l final de cada metodo
				if(con !=null) {
					con.close();
				}
				conexion.CloseConnection();

			}
			catch(SQLException e2) {
				System.out.println("Error in " + getClass().getName() + "\n" + e2);
			}
		}
		return resultado;
	}

/**
 * El m�todo consultaTiraTipo(ResourceBundle, String, String, String) obtiene la consulta de la Tira
 * en base al punto de venta, fecha y tipo de operaci�n.
 *
 * @param fecha  Fecha de consulta
 * @param pventa Punto de venta
 * @param toperacion Tipo de operaci�n
 * @param origen el parametro origen
 * @return  ArrayList valor de respuesta de la consulta
 */
	@Override
	public ArrayList consultaTiraTipo(String fecha, String pventa, String toperacion, String origen)
	{
		System.out.println("M�todo consultaTiraTipo");
		ArrayList resultado = new ArrayList();
		ArrayList resInterno = new ArrayList();
		StringBuffer lsb_query = new StringBuffer();
		PreparedStatement query = null;
		ResultSet rs = null;
		lsb_query = lsb_query.delete(0, lsb_query.length());
		lsb_query.append("SELECT D.REFERENCIA, TO_CHAR(D.FECHA_DIA,'HH24:MI') FECHA_DIA, D.CVE_OPERACION, CO.DESCRIPCION, C.CUENTA, C.FOLIO, D.IMPORTE, D.ESTADO, D.REFER_ORIG ");
		lsb_query.append("FROM TA_DIARIO D, TA_COMU_OPERACIONES CO, TA_CHEQUES C ");
		lsb_query.append("WHERE D.CVE_OPERACION = CO.CVE_OPERACION(+) ");
		lsb_query.append("AND D.CVE_PTOVTA = C.CVE_PTOVTA(+) ");
		if(IDENTIFICA_R.equals(origen)){
		lsb_query.append("AND CO.CVE_OPERACION IN('").append(INGRESO_ATS).append("','").append(EGRESO_ATS).append("') ");
		}
		else if((IDENTIFICA_CE.equals(origen))){
		lsb_query.append("AND CO.CVE_OPERACION NOT IN('").append(INGRESO_ATS).append("','").append(EGRESO_ATS).append("') ");

		}
		lsb_query.append("AND TO_CHAR(D.FECHA_DIA,'DD/MM/YYYY') = TO_CHAR(C.FECHA_DIA(+),'DD/MM/YYYY') ");
		lsb_query.append("AND D.REFERENCIA = C.REFERENCIA(+) ");
		lsb_query.append("AND D.CVE_PTOVTA = '").append(pventa).append("' ");
		lsb_query.append("AND TO_CHAR(D.FECHA_DIA,'DD/MM/YYYY') = '").append(fecha).append("' ");
		lsb_query.append("AND D.CVE_OPERACION LIKE '").append(toperacion).append("' ");
		lsb_query.append("ORDER BY D.REFER_ORIG, D.REFERENCIA  ");
		System.out.println("lsb_query: "+lsb_query.toString());
		int j = 0;
		try {
			if(con == null || con.isClosed()) {
				con = conexion.getConexion();
			}

			query = con.prepareStatement(lsb_query.toString());
			rs=query.executeQuery();
			while(rs.next()) {
				DTOTiras valores = new DTOTiras();

				origenOperacion(valores, rs.getString(CVE_OPERACION).trim());

				valores.setNoOperacion(rs.getString("REFERENCIA"));
				valores.setHora(rs.getString("FECHA_DIA"));
				if(rs.getString(DESCRIPCION)!=null && rs.getString(DESCRIPCION).length()>0) {
					valores.setTipoOperacion(rs.getString(DESCRIPCION));
				}
				else {
					valores.setTipoOperacion(rs.getString(CVE_OPERACION)!=null?rs.getString(CVE_OPERACION):"");
				}
				valores.setCuenta(rs.getString("CUENTA")!=null?rs.getString("CUENTA"):"");
				valores.setFolio(rs.getString("FOLIO")!=null?rs.getString("FOLIO"):"");
				valores.setImporte(rs.getString("IMPORTE"));
				valores.setEstado(estadoDescripcion(rs.getString("ESTADO")));
				valores.setRefer_orig(rs.getString("REFER_ORIG"));
				resInterno.add(valores);
				j++;
				if(j==Integer.parseInt(rb.getValue("ta.noregistros"))) {
					resultado.add(resInterno);
					resInterno = new ArrayList();
					j=0;
				}
			}
			if(j!=0) {
				resultado.add(resInterno);
			}
			if(resultado.size()<=0){
				resultado = esContingente(fecha, pventa);
			}
		}
		catch (SQLException e1)
		{
			System.out.println("Error in " + getClass().getName() + "\n" + e1);
		}
		finally
		{
			try
			{
				if(rs!=null) {
					rs.close();
				}
				if(query!=null) {
					query.close();
				}
//				27/06/2008 Se realiza el cierre de conexion a l final de cada metodo
				if(con !=null) {
					con.close();
				}
				conexion.CloseConnection();
			}
			catch(SQLException e2)
			{
				System.out.println("Error in " + getClass().getName() + "\n" + e2);
			}
		}
		return resultado;
	}

/**
 * El m�todo consultaTiraOperacion(String, String, String) obtiene la consulta de la Tira
 * en base al punto de venta, fecha y numero de operaci�n.
 *
 * @param fecha  Fecha de consulta
 * @param pventa Punto de venta
 * @param nooperacion N�mero de operaci�n
 * @param origen el parametro origen
 * @return  ArrayList valor de respuesta de la consulta
 */
	@Override
	public ArrayList consultaTiraOperacion(String fecha, String pventa, String nooperacion, String origen)
	{
		System.out.println("M�todo consultaTiraOperacion");
		ArrayList resultado = new ArrayList();
		ArrayList resInterno = new ArrayList();
		StringBuffer lsb_query = new StringBuffer();
		PreparedStatement query = null;
		ResultSet rs = null;
		lsb_query = lsb_query.delete(0, lsb_query.length());
		lsb_query.append("SELECT D.REFERENCIA, TO_CHAR(D.FECHA_DIA,'HH24:MI') FECHA_DIA, D.CVE_OPERACION, CO.DESCRIPCION, C.CUENTA, C.FOLIO, D.IMPORTE, D.ESTADO, D.REFER_ORIG, ");
		lsb_query.append("D.CVE_CUSTODIO, D.COD_ERROR, D.EN_LINEA, D.CVE_DIVISA, D.CVE_CUSTODIO2, D.ARRASTRE_EFVO, D.OBSERVACION  ");
		lsb_query.append("FROM TA_DIARIO D, TA_COMU_OPERACIONES CO, TA_CHEQUES C ");
		lsb_query.append("WHERE D.CVE_OPERACION = CO.CVE_OPERACION(+) ");
		if(IDENTIFICA_R.equals(origen)){
		lsb_query.append("AND REFER_ORIG IN (SELECT REFER_ORIG FROM TA_DIARIO TD WHERE TD.CVE_OPERACION IN ('").append(INGRESO_ATS).append("','").append(EGRESO_ATS).append("','").append(INGRESO_1ATS).append("','").append(EGRESO_1ATS).append("') AND TD.CVE_PTOVTA = '").append(pventa).append("' AND TO_CHAR(TD.FECHA_DIA,'DD/MM/YYYY') = '").append(fecha).append("') ");
		//lsb_query.append("AND REFER_ORIG IN (SELECT REFER_ORIG FROM TA_DIARIO TD WHERE TD.CVE_OPERACION IN ('").append(INGRESO_ATS).append("','").append(EGRESO_ATS).append("') AND TD.CVE_PTOVTA = '").append(pventa).append("' AND TO_CHAR(TD.FECHA_DIA,'DD/MM/YYYY') = '").append(fecha).append("') ");
		}
		else if((IDENTIFICA_CE.equals(origen))){
		lsb_query.append("AND REFER_ORIG NOT IN (SELECT REFER_ORIG FROM TA_DIARIO TD WHERE TD.CVE_OPERACION IN ('").append(INGRESO_ATS).append("','").append(EGRESO_ATS).append("','").append(INGRESO_1ATS).append("','").append(EGRESO_1ATS).append("') AND TD.CVE_PTOVTA = '").append(pventa).append("' AND TO_CHAR(TD.FECHA_DIA,'DD/MM/YYYY') = '").append(fecha).append("') ");
		//lsb_query.append("AND REFER_ORIG NOT IN (SELECT REFER_ORIG FROM TA_DIARIO TD WHERE TD.CVE_OPERACION IN ('").append(INGRESO_ATS).append("','").append(EGRESO_ATS).append("') AND TD.CVE_PTOVTA = '").append(pventa).append("' AND TO_CHAR(TD.FECHA_DIA,'DD/MM/YYYY') = '").append(fecha).append("') ");

		}
		lsb_query.append("AND D.CVE_PTOVTA = C.CVE_PTOVTA(+) ");
		lsb_query.append("AND TO_CHAR(D.FECHA_DIA,'DD/MM/YYYY') = TO_CHAR(C.FECHA_DIA(+),'DD/MM/YYYY') ");
		lsb_query.append("AND D.REFERENCIA = C.REFERENCIA(+) ");
		lsb_query.append("AND D.CVE_PTOVTA = '").append(pventa).append("' ");
		lsb_query.append("AND TO_CHAR(D.FECHA_DIA,'DD/MM/YYYY') = '").append(fecha).append("' ");
		lsb_query.append("AND D.REFERENCIA = '").append(nooperacion).append("' ");
		lsb_query.append("ORDER BY D.REFER_ORIG, D.REFERENCIA  ");
		System.out.println("lsb_query: "+lsb_query.toString());
		int j = 0;
		try
		{
			if(con == null || con.isClosed()) {
				con = conexion.getConexion();
			}
			query = con.prepareStatement(lsb_query.toString());
			rs=query.executeQuery();
			while(rs.next()) {
				DTOTiras valores = new DTOTiras();

				origenOperacion(valores, rs.getString(CVE_OPERACION).trim());

				valores.setNoOperacion(rs.getString("REFERENCIA"));
				valores.setHora(rs.getString("FECHA_DIA"));
//				if(rs.getString("DESCRIPCION")!=null && rs.getString("DESCRIPCION").length()>0)
//					valores.setTipoOperacion(rs.getString("DESCRIPCION"));
//				else
//					valores.setTipoOperacion(rs.getString("CVE_OPERACION")!=null?rs.getString("CVE_OPERACION"):"");
				valores.setCuenta(rs.getString("CUENTA")!=null?rs.getString("CUENTA"):"");
				valores.setFolio(rs.getString("FOLIO")!=null?rs.getString("FOLIO"):"");
				valores.setImporte(rs.getString("IMPORTE"));
				valores.setEstado(estadoDescripcion(rs.getString("ESTADO")));
				valores.setRefer_orig(rs.getString("REFER_ORIG"));
/*********************************************************************************/
				valores.setTipoOperacion(rs.getString(CVE_OPERACION)!=null?rs.getString(CVE_OPERACION):"");
				valores.setUsuario(rs.getString(CVE_CUSTODIO));
				valores.setCodError(rs.getString(COD_ERROR));
				valores.setEnLinea(rs.getString(EN_LINEA));
				valores.setCveDivisa(rs.getString(CVE_DIVISA));
				valores.setCveCustodio2(rs.getString(CVE_CUSTODIO2));
				if(rs.getString(DESCRIPCION)!=null && rs.getString(DESCRIPCION).length()>0) {
					valores.setOperacion(rs.getString(DESCRIPCION));
				}

				else {
					valores.setOperacion(valores.getTipoOperacion());
				}
				valores.setArrastre(rs.getString(ARRASTRE_EFVO));
				valores.setObservacion(rs.getString(OBSERVACION));
/**********************************************************************************/
				resInterno.add(valores);
				j++;
				if(j==Integer.parseInt(rb.getValue("ta.noregistros"))) {
					resultado.add(resInterno);
					resInterno = new ArrayList();
					j=0;
				}
			}
			if(j!=0) {
				resultado.add(resInterno);
			}
			if(resultado.size()<=0) {
				resultado = esContingente(fecha, pventa);
			}

			else {
				ArrayList cheques = consultaCheques(fecha,pventa);
				ArrayList inv_amon = consultaInv_Amon(fecha,pventa);
				int tam1 = resultado.size();

				for(int x=0;x<tam1;x++)
				{
					ArrayList pag = (ArrayList)resultado.get(x);
					int tam2 = pag.size();
					for(int y=0;y<tam2;y++) {
						DTOTiras val = (DTOTiras)pag.get(y);
						val.setCheques(registrosCheques(cheques,val.getNoOperacion()));
						val.setInv_amon(registrosInv_Amon(inv_amon,val.getNoOperacion()));
						val.setTipoOperacion(operacion(val));
						val.setCheques(null);
						val.setInv_amon(null);
					}
				}
				cheques = null;
				inv_amon = null;
			}
		}
		catch (SQLException e1) {
			System.out.println("Error in " + getClass().getName() + "\n" + e1);
		}
		finally {
			try {
				if(rs!=null) {
					rs.close();
				}
				if(query!=null) {
					query.close();
				}
				//27/06/2008 Se realiza el cierre de conexion a l final de cada metodo
				if(con !=null) {
					con.close();
				}
				conexion.CloseConnection();
			}
			catch(SQLException e2) {
				System.out.println("Error in " + getClass().getName() + "\n" + e2);
			}
		}
		return resultado;
	}

/**
 * El m�todo consultaTiraCuenta(String, String, String) obtiene la consulta de la Tira
 * en base al punto de venta, fecha y cuenta.
 *
 * @param fecha  Fecha de consulta
 * @param pventa Punto de venta
 * @param cuenta Cuenta
 * @param origen el parametro origen
 * @return  ArrayList valor de respuesta de la consulta
 */
	@Override
	public ArrayList consultaTiraCuenta(String fecha, String pventa, String cuenta, String origen)
	{
		System.out.println("M�todo consultaTiraCuenta");
		ArrayList resultado = new ArrayList();
		ArrayList resInterno = new ArrayList();
		StringBuffer lsb_query = new StringBuffer();
		PreparedStatement query = null;
		ResultSet rs = null;
		lsb_query = lsb_query.delete(0, lsb_query.length());
		lsb_query.append("SELECT D.REFERENCIA, TO_CHAR(D.FECHA_DIA,'HH24:MI') FECHA_DIA, D.CVE_OPERACION, CO.DESCRIPCION, C.FOLIO, D.IMPORTE, D.ESTADO, D.REFER_ORIG ");
		lsb_query.append("FROM TA_DIARIO D, TA_COMU_OPERACIONES CO, TA_CHEQUES C ");
		lsb_query.append("WHERE D.CVE_OPERACION = CO.CVE_OPERACION(+) ");
		if(IDENTIFICA_R.equals(origen)){
		lsb_query.append("AND REFER_ORIG IN (SELECT REFER_ORIG FROM TA_DIARIO TD WHERE TD.CVE_OPERACION IN ('").append(INGRESO_ATS).append("','").append(EGRESO_ATS).append("','").append(INGRESO_1ATS).append("','").append(EGRESO_1ATS).append("') AND TD.CVE_PTOVTA = '").append(pventa).append("' AND TO_CHAR(TD.FECHA_DIA,'DD/MM/YYYY') = '").append(fecha).append("') ");
		//lsb_query.append("AND REFER_ORIG IN (SELECT REFER_ORIG FROM TA_DIARIO TD WHERE TD.CVE_OPERACION IN ('").append(INGRESO_ATS).append("','").append(EGRESO_ATS).append("') AND TD.CVE_PTOVTA = '").append(pventa).append("' AND TO_CHAR(TD.FECHA_DIA,'DD/MM/YYYY') = '").append(fecha).append("') ");
		}
		else if((IDENTIFICA_CE.equals(origen))){
		lsb_query.append("AND REFER_ORIG NOT IN (SELECT REFER_ORIG FROM TA_DIARIO TD WHERE TD.CVE_OPERACION IN ('").append(INGRESO_ATS).append("','").append(EGRESO_ATS).append("','").append(INGRESO_1ATS).append("','").append(EGRESO_1ATS).append("') AND TD.CVE_PTOVTA = '").append(pventa).append("' AND TO_CHAR(TD.FECHA_DIA,'DD/MM/YYYY') = '").append(fecha).append("') ");
		//lsb_query.append("AND REFER_ORIG NOT IN (SELECT REFER_ORIG FROM TA_DIARIO TD WHERE TD.CVE_OPERACION IN ('").append(INGRESO_ATS).append("','").append(EGRESO_ATS).append("') AND TD.CVE_PTOVTA = '").append(pventa).append("' AND TO_CHAR(TD.FECHA_DIA,'DD/MM/YYYY') = '").append(fecha).append("') ");

		}
		lsb_query.append("AND D.CVE_PTOVTA = C.CVE_PTOVTA(+) ");
		lsb_query.append("AND TO_CHAR(D.FECHA_DIA,'DD/MM/YYYY') = TO_CHAR(C.FECHA_DIA(+),'DD/MM/YYYY') ");
		lsb_query.append("AND D.REFERENCIA = C.REFERENCIA(+) ");
		lsb_query.append("AND D.CVE_PTOVTA = '").append(pventa).append("' ");
		lsb_query.append("AND TO_CHAR(D.FECHA_DIA,'DD/MM/YYYY') = '").append(fecha).append("' ");
		lsb_query.append("AND C.CUENTA = '").append(cuenta).append("' ");
		lsb_query.append("ORDER BY D.REFER_ORIG, D.REFERENCIA  ");
		System.out.println("lsb_query: "+lsb_query.toString());
		int j = 0;
		try {
			if(con == null || con.isClosed()) {
				con = conexion.getConexion();
			}

			query = con.prepareStatement(lsb_query.toString());
			rs=query.executeQuery();
			while(rs.next()) {
				DTOTiras valores = new DTOTiras();

				origenOperacion(valores, rs.getString(CVE_OPERACION).trim());

				valores.setNoOperacion(rs.getString("REFERENCIA"));
				valores.setHora(rs.getString("FECHA_DIA"));
				if(rs.getString(DESCRIPCION)!=null && rs.getString(DESCRIPCION).length()>0) {
					valores.setTipoOperacion(rs.getString(DESCRIPCION));
				}

				else {
					valores.setTipoOperacion(rs.getString(CVE_OPERACION)!=null?rs.getString(CVE_OPERACION):"");
				}

				valores.setFolio(rs.getString("FOLIO")!=null?rs.getString("FOLIO"):"");
				valores.setImporte(rs.getString("IMPORTE"));
				valores.setEstado(estadoDescripcion(rs.getString("ESTADO")));
				valores.setRefer_orig(rs.getString("REFER_ORIG"));
				resInterno.add(valores);
				j++;
				if(j==Integer.parseInt(rb.getValue("ta.noregistros"))) {
					resultado.add(resInterno);
					resInterno = new ArrayList();
					j=0;
				}
			}
			if(j!=0)
			{
				resultado.add(resInterno);
			}
			if(resultado.size()<=0){
				resultado = esContingente(fecha, pventa);
			}

		}
		catch (SQLException e1) {
			System.out.println("Error in " + getClass().getName() + "\n" + e1);
		}
		finally {
			try {
				if(rs!=null) {
					rs.close();
				}
				if(query!=null) {
					query.close();
				}
				//27/06/2008 Se realiza el cierre de conexion a l final de cada metodo
				if(con !=null) {
					con.close();
				}
				conexion.CloseConnection();
			}
			catch(SQLException e2)
			{
				System.out.println("Error in " + getClass().getName() + "\n" + e2);
			}
		}
		return resultado;
	}

/**
 * El m�todo consultaTiraUsuario(String, String, String) obtiene la consulta de la Tira
 * en base al punto de venta, fecha y usuario.
 *
 * @param fecha  Fecha de consulta
 * @param pventa Punto de venta
 * @param usuario Usuario
 * @param origen el parametro origen
 * @return  ArrayList valor de respuesta de la consulta
 */
	@Override
	public ArrayList consultaTiraUsuario(String fecha, String pventa, String usuario, String origen)
	{
		System.out.println("M�todo consultaTiraUsuario");
		ArrayList resultado = new ArrayList();
		ArrayList resInterno = new ArrayList();
		StringBuffer lsb_query = new StringBuffer();
		PreparedStatement query = null;
		ResultSet rs = null;
		lsb_query = lsb_query.delete(0, lsb_query.length());
		lsb_query.append("SELECT D.REFERENCIA, TO_CHAR(D.FECHA_DIA,'HH24:MI') FECHA_DIA, D.CVE_OPERACION, CO.DESCRIPCION, D.CVE_CUSTODIO, ");
		lsb_query.append("D.COD_ERROR, D.EN_LINEA, D.CVE_DIVISA, D.IMPORTE, D.ESTADO, D.CVE_CUSTODIO2, D.ARRASTRE_EFVO, D.OBSERVACION, D.REFER_ORIG ");
		lsb_query.append("FROM TA_DIARIO D, TA_COMU_OPERACIONES CO ");
		lsb_query.append("WHERE D.CVE_OPERACION = CO.CVE_OPERACION(+) ");
		if(IDENTIFICA_R.equals(origen)){
		lsb_query.append("AND REFER_ORIG IN (SELECT REFER_ORIG FROM TA_DIARIO TD WHERE TD.CVE_OPERACION IN ('").append(INGRESO_ATS).append("','").append(EGRESO_ATS).append("','").append(INGRESO_1ATS).append("','").append(EGRESO_1ATS).append("') AND TD.CVE_PTOVTA = '").append(pventa).append("' AND TO_CHAR(TD.FECHA_DIA,'DD/MM/YYYY') = '").append(fecha).append("') ");
		//lsb_query.append("AND REFER_ORIG IN (SELECT REFER_ORIG FROM TA_DIARIO TD WHERE TD.CVE_OPERACION IN ('").append(INGRESO_ATS).append("','").append(EGRESO_ATS).append("') AND TD.CVE_PTOVTA = '").append(pventa).append("' AND TO_CHAR(TD.FECHA_DIA,'DD/MM/YYYY') = '").append(fecha).append("') ");
		}
		else if((IDENTIFICA_CE.equals(origen))){
		lsb_query.append("AND REFER_ORIG NOT IN (SELECT REFER_ORIG FROM TA_DIARIO TD WHERE TD.CVE_OPERACION IN ('").append(INGRESO_ATS).append("','").append(EGRESO_ATS).append("','").append(INGRESO_1ATS).append("','").append(EGRESO_1ATS).append("') AND TD.CVE_PTOVTA = '").append(pventa).append("' AND TO_CHAR(TD.FECHA_DIA,'DD/MM/YYYY') = '").append(fecha).append("') ");
		//lsb_query.append("AND REFER_ORIG NOT IN (SELECT REFER_ORIG FROM TA_DIARIO TD WHERE TD.CVE_OPERACION IN ('").append(INGRESO_ATS).append("','").append(EGRESO_ATS).append("') AND TD.CVE_PTOVTA = '").append(pventa).append("' AND TO_CHAR(TD.FECHA_DIA,'DD/MM/YYYY') = '").append(fecha).append("') ");

		}
		lsb_query.append("AND D.CVE_PTOVTA = '").append(pventa).append("' ");
		lsb_query.append("AND TO_CHAR(D.FECHA_DIA,'DD/MM/YYYY') = '").append(fecha).append("' ");
		lsb_query.append("AND CVE_CUSTODIO = '").append(usuario).append("' ");
		lsb_query.append("ORDER BY D.REFER_ORIG, D.REFERENCIA  ");
		System.out.println("lsb_query: "+lsb_query.toString());
		int j = 0;
		try {
			if(con == null || con.isClosed()) {
				con = conexion.getConexion();
			}
			query = con.prepareStatement(lsb_query.toString());
			rs=query.executeQuery();
			while(rs.next()) {
				DTOTiras valores = new DTOTiras();

				origenOperacion(valores, rs.getString(CVE_OPERACION).trim());

				valores.setNoOperacion(rs.getString("REFERENCIA"));
				valores.setHora(rs.getString("FECHA_DIA"));
				valores.setTipoOperacion(rs.getString(CVE_OPERACION)!=null?rs.getString(CVE_OPERACION):"");
				valores.setUsuario(rs.getString(CVE_CUSTODIO));
				valores.setCodError(rs.getString(COD_ERROR));
				valores.setEnLinea(rs.getString(EN_LINEA));
				valores.setCveDivisa(rs.getString(CVE_DIVISA));
				valores.setImporte(rs.getString("IMPORTE"));
				valores.setEstado(rs.getString("ESTADO"));
				valores.setCveCustodio2(rs.getString(CVE_CUSTODIO2));
				valores.setArrastre(rs.getString(ARRASTRE_EFVO));
				if(rs.getString(DESCRIPCION)!=null && rs.getString(DESCRIPCION).length()>0) {
					valores.setOperacion(rs.getString(DESCRIPCION));
				}

				else {
					valores.setOperacion(valores.getTipoOperacion());
				}

				valores.setObservacion(rs.getString(OBSERVACION));
				valores.setRefer_orig(rs.getString("REFER_ORIG"));
				resInterno.add(valores);
				j++;
				if(j==Integer.parseInt(rb.getValue("ta.noregistros"))) {
					resultado.add(resInterno);
					resInterno = new ArrayList();
					j=0;
				}
			}

			if(j!=0) {
				resultado.add(resInterno);
			}
			if(resultado.size()<=0) {
				resultado = esContingente(fecha, pventa);
			}
			else {
				ArrayList cheques = consultaCheques(fecha,pventa);
				ArrayList inv_amon = consultaInv_Amon(fecha,pventa);
				int tam1 = resultado.size();
				for(int x=0;x<tam1;x++) {
					ArrayList pag = (ArrayList)resultado.get(x);
					int tam2 = pag.size();
					for(int y=0;y<tam2;y++) {
						DTOTiras val = (DTOTiras)pag.get(y);
						val.setCheques(registrosCheques(cheques,val.getNoOperacion()));
						val.setInv_amon(registrosInv_Amon(inv_amon,val.getNoOperacion()));
						val.setOperacion(operacion(val));
						val.setCheques(null);
						val.setInv_amon(null);
					}
				}
				cheques = null;
				inv_amon = null;
			}
		}
		catch (SQLException e1)
		{
			System.out.println("Error in " + getClass().getName() + "\n" + e1);
		}
		finally
		{
			try
			{
				if(rs!=null) {
					rs.close();
				}
				if(query!=null) {
					query.close();
				}
				//27/06/2008 Se realiza el cierre de conexion a l final de cada metodo
				if(con !=null) {
					con.close();
				}
				conexion.CloseConnection();

			}
			catch(SQLException e2)
			{
				System.out.println("Error in " + getClass().getName() + "\n" + e2);
			}
		}
		return resultado;
	}

/**
 * El m�todo consultaTiraHora(String, String, String, String) obtiene la consulta de la Tira
 * en base al punto de venta, fecha, hora inicial y hora final.
 *
 * @param fecha  Fecha de consulta
 * @param pventa Punto de venta
 * @param horaini Hora inicial
 * @param horafin Hora final
 * @param origen el parametro origen
 * @return  ArrayList valor de respuesta de la consulta
 */
	@Override
	public ArrayList consultaTiraHora(String fecha, String pventa, String horaini, String horafin, String origen)
	{
		System.out.println("M�todo consultaTiraHora");
		ArrayList resultado = new ArrayList();
		ArrayList resInterno = new ArrayList();
		StringBuffer lsb_query = new StringBuffer();
		PreparedStatement query = null;
		ResultSet rs = null;
		lsb_query = lsb_query.delete(0, lsb_query.length());
		lsb_query.append("SELECT D.REFERENCIA, TO_CHAR(D.FECHA_DIA,'HH24:MI') FECHA_DIA, D.CVE_OPERACION, CO.DESCRIPCION, C.CUENTA, C.FOLIO, D.IMPORTE, D.ESTADO, D.REFER_ORIG ");
		lsb_query.append("FROM TA_DIARIO D, TA_COMU_OPERACIONES CO, TA_CHEQUES C ");
		lsb_query.append("WHERE D.CVE_OPERACION = CO.CVE_OPERACION(+) ");
		if(IDENTIFICA_R.equals(origen)){
		lsb_query.append("AND REFER_ORIG IN (SELECT REFER_ORIG FROM TA_DIARIO TD WHERE TD.CVE_OPERACION IN ('").append(INGRESO_ATS).append("','").append(EGRESO_ATS).append("','").append(INGRESO_1ATS).append("','").append(EGRESO_1ATS).append("') AND TD.CVE_PTOVTA = '").append(pventa).append("' AND TO_CHAR(TD.FECHA_DIA,'DD/MM/YYYY') = '").append(fecha).append("') ");
		//lsb_query.append("AND REFER_ORIG IN (SELECT REFER_ORIG FROM TA_DIARIO TD WHERE TD.CVE_OPERACION IN ('").append(INGRESO_ATS).append("','").append(EGRESO_ATS).append("') AND TD.CVE_PTOVTA = '").append(pventa).append("' AND TO_CHAR(TD.FECHA_DIA,'DD/MM/YYYY') = '").append(fecha).append("') ");
		}
		else if((IDENTIFICA_CE.equals(origen))){
		lsb_query.append("AND REFER_ORIG NOT IN (SELECT REFER_ORIG FROM TA_DIARIO TD WHERE TD.CVE_OPERACION IN ('").append(INGRESO_ATS).append("','").append(EGRESO_ATS).append("','").append(INGRESO_1ATS).append("','").append(EGRESO_1ATS).append("') AND TD.CVE_PTOVTA = '").append(pventa).append("' AND TO_CHAR(TD.FECHA_DIA,'DD/MM/YYYY') = '").append(fecha).append("') ");
		//lsb_query.append("AND REFER_ORIG NOT IN (SELECT REFER_ORIG FROM TA_DIARIO TD WHERE TD.CVE_OPERACION IN ('").append(INGRESO_ATS).append("','").append(EGRESO_ATS).append("') AND TD.CVE_PTOVTA = '").append(pventa).append("' AND TO_CHAR(TD.FECHA_DIA,'DD/MM/YYYY') = '").append(fecha).append("') ");

		}
		lsb_query.append("AND D.CVE_PTOVTA = C.CVE_PTOVTA(+) ");
		lsb_query.append("AND TO_CHAR(D.FECHA_DIA,'DD/MM/YYYY') = TO_CHAR(C.FECHA_DIA(+),'DD/MM/YYYY') ");
		lsb_query.append("AND D.REFERENCIA = C.REFERENCIA(+) ");
		lsb_query.append("AND D.CVE_PTOVTA = '").append(pventa).append("' ");
		lsb_query.append("AND D.FECHA_DIA >= TO_DATE('").append(fecha+" "+horaini).append("','DD/MM/YYYY HH24:MI') ");
		lsb_query.append("AND D.FECHA_DIA <= TO_DATE('").append(fecha+" "+horafin).append("','DD/MM/YYYY HH24:MI') ");
		lsb_query.append("ORDER BY D.REFER_ORIG, D.REFERENCIA  ");
		System.out.println("lsb_query: "+lsb_query.toString());
		int j = 0;
		try
		{
			if(con == null || con.isClosed()){
				con = conexion.getConexion();
			}

			query = con.prepareStatement(lsb_query.toString());
			rs=query.executeQuery();
			while(rs.next()) {
				DTOTiras valores = new DTOTiras();

				origenOperacion(valores, rs.getString(CVE_OPERACION).trim());

				valores.setNoOperacion(rs.getString("REFERENCIA"));
				valores.setHora(rs.getString("FECHA_DIA"));
				if(rs.getString(DESCRIPCION)!=null && rs.getString(DESCRIPCION).length()>0) {
					valores.setTipoOperacion(rs.getString(DESCRIPCION));
				}

				else {
					valores.setTipoOperacion(rs.getString(CVE_OPERACION)!=null?rs.getString(CVE_OPERACION):"");
				}

				valores.setCuenta(rs.getString("CUENTA")!=null?rs.getString("CUENTA"):"");
				valores.setFolio(rs.getString("FOLIO")!=null?rs.getString("FOLIO"):"");
				valores.setImporte(rs.getString("IMPORTE"));
				valores.setEstado(estadoDescripcion(rs.getString("ESTADO")));
				valores.setRefer_orig(rs.getString("REFER_ORIG"));
				resInterno.add(valores);
				j++;
				if(j==Integer.parseInt(rb.getValue("ta.noregistros"))) {
					resultado.add(resInterno);
					resInterno = new ArrayList();
					j=0;
				}
			}
			if(j!=0) {
				resultado.add(resInterno);
			}

			if(resultado.size()<=0){
				resultado = esContingente(fecha, pventa);
			}

		}
		catch (SQLException e1) {
			System.out.println("Error in " + getClass().getName() + "\n" + e1);
		}
		finally {
			try {
				if(rs!=null) {
					rs.close();
				}
				if(query!=null) {
					query.close();
				}
				//27/06/2008 Se realiza el cierre de conexion a l final de cada metodo
				if(con !=null) {
					con.close();
				}
				conexion.CloseConnection();
			}
			catch(SQLException e2) {
				System.out.println("Error in " + getClass().getName() + "\n" + e2);
			}
		}
		return resultado;
	}

/**
 * El m�todo consultaTiraImporte(String, String, String, String) obtiene la consulta de la Tira
 * en base al punto de venta, fecha, importe inicial y importe final.
 *
 * @param fecha  Fecha de consulta
 * @param pventa Punto de venta
 * @param importeini Importe inicial
 * @param importefin Importe final
 * @param origen el parametro origen
 * @return  ArrayList valor de respuesta de la consulta
 */
	@Override
	public ArrayList consultaTiraImporte(String fecha, String pventa, String importeini, String importefin, String origen)
	{
		System.out.println("M�todo consultaTiraImporte");
		ArrayList resultado = new ArrayList();
		ArrayList resInterno = new ArrayList();
		StringBuffer lsb_query = new StringBuffer();
		PreparedStatement query = null;
		ResultSet rs = null;
		lsb_query = lsb_query.delete(0, lsb_query.length());
		lsb_query.append("SELECT D.REFERENCIA, TO_CHAR(D.FECHA_DIA,'HH24:MI') FECHA_DIA, D.CVE_OPERACION, CO.DESCRIPCION, C.CUENTA, C.FOLIO, D.IMPORTE, D.ESTADO, D.REFER_ORIG ");
		lsb_query.append("FROM TA_DIARIO D, TA_COMU_OPERACIONES CO, TA_CHEQUES C ");
		lsb_query.append("WHERE D.CVE_OPERACION = CO.CVE_OPERACION(+) ");
		if(IDENTIFICA_R.equals(origen)){
		lsb_query.append("AND REFER_ORIG IN (SELECT REFER_ORIG FROM TA_DIARIO TD WHERE TD.CVE_OPERACION IN ('").append(INGRESO_ATS).append("','").append(EGRESO_ATS).append("','").append(INGRESO_1ATS).append("','").append(EGRESO_1ATS).append("') AND TD.CVE_PTOVTA = '").append(pventa).append("' AND TO_CHAR(TD.FECHA_DIA,'DD/MM/YYYY') = '").append(fecha).append("') ");
		//lsb_query.append("AND REFER_ORIG IN (SELECT REFER_ORIG FROM TA_DIARIO TD WHERE TD.CVE_OPERACION IN ('").append(INGRESO_ATS).append("','").append(EGRESO_ATS).append("') AND TD.CVE_PTOVTA = '").append(pventa).append("' AND TO_CHAR(TD.FECHA_DIA,'DD/MM/YYYY') = '").append(fecha).append("') ");
		}
		else if((IDENTIFICA_CE.equals(origen))){
		lsb_query.append("AND REFER_ORIG NOT IN (SELECT REFER_ORIG FROM TA_DIARIO TD WHERE TD.CVE_OPERACION IN ('").append(INGRESO_ATS).append("','").append(EGRESO_ATS).append("','").append(INGRESO_1ATS).append("','").append(EGRESO_1ATS).append("') AND TD.CVE_PTOVTA = '").append(pventa).append("' AND TO_CHAR(TD.FECHA_DIA,'DD/MM/YYYY') = '").append(fecha).append("') ");
		//lsb_query.append("AND REFER_ORIG NOT IN (SELECT REFER_ORIG FROM TA_DIARIO TD WHERE TD.CVE_OPERACION IN ('").append(INGRESO_ATS).append("','").append(EGRESO_ATS).append("') AND TD.CVE_PTOVTA = '").append(pventa).append("' AND TO_CHAR(TD.FECHA_DIA,'DD/MM/YYYY') = '").append(fecha).append("') ");

		}
		lsb_query.append("AND D.CVE_PTOVTA = C.CVE_PTOVTA(+) ");
		lsb_query.append("AND TO_CHAR(D.FECHA_DIA,'DD/MM/YYYY') = TO_CHAR(C.FECHA_DIA(+),'DD/MM/YYYY') ");
		lsb_query.append("AND D.REFERENCIA = C.REFERENCIA(+) ");
		lsb_query.append("AND D.CVE_PTOVTA = '").append(pventa).append("' ");
		lsb_query.append("AND TO_CHAR(D.FECHA_DIA,'DD/MM/YYYY') = '").append(fecha).append("' ");
		lsb_query.append("AND D.IMPORTE >= ").append(importeini).append(" ");
		lsb_query.append("AND D.IMPORTE <= ").append(importefin).append(" ");
		lsb_query.append("ORDER BY D.REFER_ORIG, D.REFERENCIA  ");
		System.out.println("lsb_query: "+lsb_query.toString());
		int j = 0;
		try
		{
			if(con == null || con.isClosed()) {
				con = conexion.getConexion();
			}

			query = con.prepareStatement(lsb_query.toString());
			rs=query.executeQuery();
			while(rs.next()) {
				DTOTiras valores = new DTOTiras();

				origenOperacion(valores, rs.getString(CVE_OPERACION).trim());

				valores.setNoOperacion(rs.getString("REFERENCIA"));
				valores.setHora(rs.getString("FECHA_DIA"));
				if(rs.getString(DESCRIPCION)!=null && rs.getString(DESCRIPCION).length()>0) {
					valores.setTipoOperacion(rs.getString(DESCRIPCION));
				}

				else {
					valores.setTipoOperacion(rs.getString(CVE_OPERACION)!=null?rs.getString(CVE_OPERACION):"");
				}

				valores.setCuenta(rs.getString("CUENTA")!=null?rs.getString(CVE_OPERACION):"");
				valores.setFolio(rs.getString("FOLIO")!=null?rs.getString(CVE_OPERACION):"");
				valores.setImporte(rs.getString("IMPORTE"));
				valores.setEstado(estadoDescripcion(rs.getString("ESTADO")));
				valores.setRefer_orig(rs.getString("REFER_ORIG"));
				resInterno.add(valores);
				j++;
				if(j==Integer.parseInt(rb.getValue("ta.noregistros"))) {
					resultado.add(resInterno);
					resInterno = new ArrayList();
					j=0;
				}
			}
			if(j!=0) {
				resultado.add(resInterno);
			}
			if(resultado.size()<=0) {
				resultado = esContingente(fecha, pventa);
			}

		}
		catch (SQLException e1) {
			System.out.println("Error in " + getClass().getName() + "\n" + e1);
		}
		finally {
			try {
				if(rs!=null) {
					rs.close();
				}
				if(query!=null) {
					query.close();
				}
				//27/06/2008 Se realiza el cierre de conexion a l final de cada metodo
				if(con !=null) {
					con.close();
				}
				conexion.CloseConnection();

			}
			catch(SQLException e2)
			{
				System.out.println("Error in " + getClass().getName() + "\n" + e2);
			}
		}
		return resultado;
	}


	/**
	 * El m�todo consultaTiraIdAts(String, String, String, String) obtiene la consulta de la Tira
	 * en base a la fecha, punto de venta, identificador de Ats y origen.
	 *
	 * @param fecha  Fecha de consulta
	 * @param pventa Punto de venta
	 * @param idAts identificador de ATS
	 * @param origen Cajonera o ATS
	 * @return  ArrayList valor de respuesta de la consulta
	 */
	@Override
		public ArrayList consultaTiraIdAts(String fecha, String pventa, String idAts, String origen)
		{
			System.out.println("Metodo consultaTiraIdAts");
			ArrayList resultado = new ArrayList();
			ArrayList resInterno = new ArrayList();
			StringBuffer lsb_query = new StringBuffer();
			PreparedStatement query = null;
			ResultSet rs = null;
			lsb_query = lsb_query.delete(0, lsb_query.length());
			lsb_query = lsb_query.delete(0, lsb_query.length());
			lsb_query.append("SELECT D.REFERENCIA, TO_CHAR(D.FECHA_DIA,'HH24:MI') FECHA_DIA, D.CVE_OPERACION, CO.DESCRIPCION, C.CUENTA, C.FOLIO, D.IMPORTE, D.ESTADO, D.REFER_ORIG,   D.COD_ERROR, D.EN_LINEA, D.CVE_DIVISA, D.IMPORTE, D.ESTADO, D.CVE_CUSTODIO2, D.ARRASTRE_EFVO, D.OBSERVACION, D.REFER_ORIG ");
			lsb_query.append("FROM TA_DIARIO D, TA_COMU_OPERACIONES CO, TA_CHEQUES C ");
			lsb_query.append("WHERE D.CVE_OPERACION = CO.CVE_OPERACION(+) ");
			lsb_query.append("AND D.CVE_PTOVTA = C.CVE_PTOVTA(+) ");
			lsb_query.append("AND D.REFERENCIA = C.REFERENCIA(+) ");
			lsb_query.append("AND TO_CHAR(D.FECHA_DIA,'DD/MM/YYYY') = TO_CHAR(C.FECHA_DIA(+),'DD/MM/YYYY') ");
			if(IDENTIFICA_R.equals(origen)) {
				lsb_query.append("AND REFER_ORIG IN (SELECT REFER_ORIG FROM TA_DIARIO TD WHERE ");
				lsb_query.append("	TD.CVE_CUSTODIO2 = '").append(idAts).append("' ");
				lsb_query.append("	AND D.CVE_PTOVTA = '").append(pventa).append("' ");
				lsb_query.append("	AND TO_CHAR(D.FECHA_DIA,'DD/MM/YYYY') = '").append(fecha).append("') ");
				//lsb_query.append("	TD.CVE_CUSTODIO = '").append(idAts).append("' ");
			}else{
				lsb_query.append("AND D.CVE_CUSTODIO2 = '").append(idAts).append("' ");
				//lsb_query.append("AND D.CVE_CUSTODIO = '").append(idAts).append("' ");
			}
			lsb_query.append("AND D.CVE_PTOVTA = '").append(pventa).append("' ");
			lsb_query.append("AND TO_CHAR(D.FECHA_DIA,'DD/MM/YYYY') = '").append(fecha).append("' ");
			lsb_query.append("ORDER BY D.REFER_ORIG, D.REFERENCIA  ");
			System.out.println("lsb_query: "+lsb_query.toString());
			int j = 0;
			try {
				if(con == null || con.isClosed()) {
					con = conexion.getConexion();
				}

				query = con.prepareStatement(lsb_query.toString());
				rs=query.executeQuery();

				while(rs.next()) {
					DTOTiras valores = new DTOTiras();
					valores.setNoOperacion(rs.getString("REFERENCIA"));
					valores.setHora(rs.getString("FECHA_DIA"));
					if(rs.getString(DESCRIPCION)!=null && rs.getString(DESCRIPCION).length()>0){
						valores.setTipoOperacion(rs.getString(DESCRIPCION));
					}
					else {
						valores.setTipoOperacion(rs.getString(CVE_OPERACION)!=null?rs.getString(CVE_OPERACION):"");
					}

					valores.setCuenta(rs.getString("CUENTA")!=null?rs.getString("CUENTA"):"");
					valores.setFolio(rs.getString("FOLIO")!=null?rs.getString("FOLIO"):"");
					valores.setImporte(rs.getString("IMPORTE"));
					valores.setEstado(estadoDescripcion(rs.getString("ESTADO")));
					valores.setRefer_orig(rs.getString("REFER_ORIG"));

					valores.setCodError(rs.getString(COD_ERROR));
					valores.setArrastre(rs.getString(ARRASTRE_EFVO));
					valores.setEnLinea(rs.getString(EN_LINEA));
					valores.setCveDivisa(rs.getString(CVE_DIVISA));
					valores.setCveCustodio2(rs.getString(CVE_CUSTODIO2));
					valores.setObservacion(rs.getString(OBSERVACION));
					if(rs.getString(DESCRIPCION)!=null && rs.getString(DESCRIPCION).length()>0) {
						valores.setOperacion(rs.getString(DESCRIPCION));
					}

					else {
						valores.setOperacion(valores.getTipoOperacion());
					}


					resInterno.add(valores);
					j++;
					if(j==Integer.parseInt(rb.getValue("ta.noregistros"))) {
						resultado.add(resInterno);
						resInterno = new ArrayList();
						j=0;
					}
				}
				if(j!=0) {
					resultado.add(resInterno);
				}

				if(rs!=null) {
					rs.close();
				}
				if(query!=null) {
					query.close();
				}
				if(resultado.size()<=0) {
					resultado = new ArrayList();
					resultado.add("No existen movimientos con el ATS seleccionado");
				}
				else {
					ArrayList cheques = consultaCheques(fecha,pventa);
					ArrayList inv_amon = consultaInv_Amon(fecha,pventa);
					int tam1 = resultado.size();
					for(int x=0;x<tam1;x++) {
						ArrayList pag = (ArrayList)resultado.get(x);
						int tam2 = pag.size();
						for(int y=0;y<tam2;y++) {
							DTOTiras val = (DTOTiras)pag.get(y);
							val.setCheques(registrosCheques(cheques,val.getNoOperacion()));
							val.setInv_amon(registrosInv_Amon(inv_amon,val.getNoOperacion()));
							val.setTipoOperacion(operacion(val));
							val.setCheques(null);
							val.setInv_amon(null);
						}
					}
					cheques = null;
					inv_amon = null;
				}
			}
			catch (SQLException e1) {
				System.out.println("Error in " + getClass().getName() + "\n" + e1);
			}

			finally {

				try {
				if(rs!=null) {
					rs.close();
				}

				if(query!=null) {
					query.close();
				}
				if(con !=null) {
					con.close();
				}
					conexion.CloseConnection();
			}
				catch(SQLException e2) {
					System.out.println("Error in " + getClass().getName() + "\n" + e2);
				}
			}
			return resultado;
		}

	/**
	 * Metodo encargado del obtener el origen de una operacion.
	 *
	 * @param tira
	 *            el parametro tira en donde se va a guardar el origen
	 * @param tipoOperacion
	 *            el parametro operacion que contiene el tipo de operacion que
	 *            se va a evaluar
	 */
	@Override
	public void origenOperacion(DTOTiras tira, String tipoOperacion) {

		if (INGRESO_ATS.equalsIgnoreCase(tipoOperacion)
				|| EGRESO_ATS.equalsIgnoreCase(tipoOperacion)) {
			tira.setOrigen(IDENTIFICA_R);
		} else if (INGRESO_CAJ.equalsIgnoreCase(tipoOperacion)
				|| EGRESO_CAJ.equalsIgnoreCase(tipoOperacion)) {
			tira.setOrigen(IDENTIFICA_CE);
		} else {
			tira.setOrigen(IDENTIFICA_V);
		}
	}

/**
 * El m�todo makePdf(int, ArrayList, String, String) genera el PDF.
 *
 * @param seleccion  criterio de busqueda
 * @param datos Valores que se insertara en el PDF
 * @param dato1 Valor que se insertara en el encabezado
 * @param dato2 Valor que se insertara en el encabezado
 * @param encabeza el parametro encabeza
 * @param origen el parametro origen
 * @return ByteArrayOutputStream El PDF
 * origen Valor que identifica si el registro es cajonera o identificador ATS
 */
	@Override
	public ByteArrayOutputStream makePdf(int seleccion, ArrayList datos, String dato1, String dato2, String encabeza, String origen)
	{
		System.out.println("M�todo makePdf");
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		Font black = FontFactory.getFont(FontFactory.HELVETICA, 8, Font.NORMAL, new BaseColor(0x00, 0x00, 0x00));
		DecimalFormat dec = new DecimalFormat("#,##0.00");
		Rectangle hoja = null;
		if(seleccion== 0 || seleccion == 4)
			hoja = PageSize.LETTER;
		else
			hoja = PageSize.LETTER.rotate();
		Document document = new Document(hoja,10,10,10,10);
		try
		{
			// create simple doc and write to a ByteArrayOutputStream
			PdfWriter.getInstance(document, baos);
			document.open();
			// Crear el encabezado del PDF
			PdfPTable datatable = creaEncabezado(seleccion,dato1,dato2, encabeza,origen);
			datatable.getDefaultCell().setBorderWidth(1);
			// Opcion de como llenar el PDF segun sea el caso
			switch(seleccion)
			{
				// Datos que se pintan en la consulta general
				case 0:
					for (int i=0;i<datos.size();i++)
					{
						if (i%2 == 0) {
							datatable.getDefaultCell().setBackgroundColor(new BaseColor(0xFF, 0xFF, 0xFF));
						}else {
							datatable.getDefaultCell().setBackgroundColor(new BaseColor(0xEB, 0xEB, 0xEB));
						}
						DTOTiras registro = (DTOTiras)datos.get(i);
						if(registro.getRefer_orig().equals("0") || registro.getNoOperacion().equals(registro.getRefer_orig())){
							datatable.addCell(new Paragraph(registro.getNoOperacion(),black));
						}
						else {
						datatable.addCell(new Paragraph(registro.getNoOperacion()+"/"+registro.getRefer_orig(),black));
						}

						datatable.addCell(new Paragraph(registro.getHora(),black));
						datatable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
						datatable.addCell(new Paragraph(registro.getOperacion(),black));
						datatable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
						datatable.addCell(new Paragraph(registro.getUsuario(),black));

						if("".equals(origen.trim())){
						datatable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
						datatable.addCell(new Paragraph(registro.getOrigen(),black));
						}
					}
				break;
				// Datos que se pintan en la consulta por importe, hora, operacion
				case 1:
				case 2:
				case 5:
				case 6:
				case 7:
					int tam1 = datos.size();
					for (int i=0;i<tam1;i++)
					{
						if (i%2 == 0)
						{
							datatable.getDefaultCell().setBackgroundColor(new BaseColor(0xFF, 0xFF, 0xFF));
						}else
						{
							datatable.getDefaultCell().setBackgroundColor(new BaseColor(0xEB, 0xEB, 0xEB));
						}
						DTOTiras registro = (DTOTiras)datos.get(i);
						if(registro.getRefer_orig().equals("0") || registro.getNoOperacion().equals(registro.getRefer_orig()))
							datatable.addCell(new Paragraph(registro.getNoOperacion(),black));
						else
							datatable.addCell(new Paragraph(registro.getNoOperacion()+"/"+registro.getRefer_orig(),black));
						datatable.addCell(new Paragraph(registro.getHora(),black));
						datatable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
						datatable.addCell(new Paragraph(registro.getTipoOperacion(),black));
						datatable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
						datatable.addCell(new Paragraph(registro.getCuenta(),black));
						datatable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
						datatable.addCell(new Paragraph(registro.getFolio(),black));
						datatable.addCell(new Paragraph(dec.format(Double.parseDouble(registro.getImporte())),black));
						datatable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
						datatable.addCell(new Paragraph(registro.getEstado(),black));
					}
				break;
				// Datos que se pintan en la consulta por cuenta
				case 3:
					int tam2 = datos.size();
					for (int i=0;i<tam2;i++)
					{
						if (i%2 == 0)
						{
							datatable.getDefaultCell().setBackgroundColor(new BaseColor(0xFF, 0xFF, 0xFF));
						}else
						{
							datatable.getDefaultCell().setBackgroundColor(new BaseColor(0xEB, 0xEB, 0xEB));
						}
						DTOTiras registro = (DTOTiras)datos.get(i);
						if(registro.getRefer_orig().equals("0") || registro.getNoOperacion().equals(registro.getRefer_orig()))
							datatable.addCell(new Paragraph(registro.getNoOperacion(),black));
						else
							datatable.addCell(new Paragraph(registro.getNoOperacion()+"/"+registro.getRefer_orig(),black));
						datatable.addCell(new Paragraph(registro.getHora(),black));
						datatable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
						datatable.addCell(new Paragraph(registro.getTipoOperacion(),black));
						datatable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
						datatable.addCell(new Paragraph(registro.getFolio(),black));
						datatable.addCell(new Paragraph(dec.format(Double.parseDouble(registro.getImporte())),black));
						datatable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
						datatable.addCell(new Paragraph(registro.getEstado(),black));
					}
				break;
				// Datos que se pintan en la consulta por usuario
				case 4:
					int tam3 = datos.size();
					for (int i=0;i<tam3;i++)
					{
						if (i%2 == 0)
						{
							datatable.getDefaultCell().setBackgroundColor(new BaseColor(0xFF, 0xFF, 0xFF));
						}else
						{
							datatable.getDefaultCell().setBackgroundColor(new BaseColor(0xEB, 0xEB, 0xEB));
						}
						DTOTiras registro = (DTOTiras)datos.get(i);
						if(registro.getRefer_orig().equals("0") || registro.getNoOperacion().equals(registro.getRefer_orig()))
							datatable.addCell(new Paragraph(registro.getNoOperacion(),black));
						else
							datatable.addCell(new Paragraph(registro.getNoOperacion()+"/"+registro.getRefer_orig(),black));
						datatable.addCell(new Paragraph(registro.getHora(),black));
						datatable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
						datatable.addCell(new Paragraph(registro.getOperacion(),black));
						datatable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
					}
				break;
				default:
				// Este caso nunca aplicara
					System.out.println("No aplica esta seleccion.");
				break;
			}
			document.add(datatable);
			document.close();
		}
		catch (Exception e2)
		{
			System.out.println("Error in " + getClass().getName() + "\n" + e2);
		}
		return baos;
	}

/**
 * El m�todo creaEncabezado(int, String, String) genera el encabezado del PDF.
 *
 * @param seleccion  criterio de busqueda
 * @param dato1 Valor que se insertara en el encabezado
 * @param dato2 Valor que se insertara en el encabezado
 * @param encabeza el parametro encabeza
 * @param origen el parametro origen
 * @return PdfPTable Encabezado del PDF
 */
	@Override
	public PdfPTable creaEncabezado(int seleccion, String dato1, String dato2, String encabeza, String origen)
	{
		System.out.println("Metodo creaEncabezado");
		PdfPTable datatable = null;
		Font white = FontFactory.getFont(FontFactory.HELVETICA, Font.DEFAULTSIZE, Font.BOLD, new BaseColor(0xFF, 0xFF, 0xFF));
		Font whiteG = FontFactory.getFont(FontFactory.HELVETICA, 11, Font.BOLD, new BaseColor(0xFF, 0xFF, 0xFF));
		int columnas = 0;
		Paragraph titulo = null;
		try
		{
			// Se coloca el titulo segun sea el caso de busqueda
			switch(seleccion)
			{
				case 0:
					if("".equals(origen.trim())){
					columnas = 5;
				}else {
					columnas = 4;

				}
					titulo = new Paragraph(encabeza+"\nRastreo por Punto de Venta \""+dato1+"\" en la Fecha \""+dato2+"\" ",whiteG);
				break;
				case 1:
					columnas = 7;
					titulo = new Paragraph(encabeza+"\nRastreo por Tipo de Operaci�n \""+dato1+"\" ",whiteG);
				break;
				case 2:
					columnas = 7;
					titulo = new Paragraph(encabeza+"\nRastreo por N�mero de Operaci�n \""+dato1+"\" ",whiteG);
				break;
				case 3:
					columnas = 6;
					titulo = new Paragraph(encabeza+"\nRastreo de Operaciones por Cuenta \""+dato1+"\" ",whiteG);
				break;
				case 4:
					columnas = 3;
					titulo = new Paragraph(encabeza+"\nRastreo por Usuario \""+dato1+"-"+descCustodio(dato1)+"\" ",whiteG);
				break;
				case 5:
					columnas = 7;
					titulo = new Paragraph(encabeza+"\nRastreo por Horario desde \""+dato1+"\" hasta \""+dato2+"\" ",whiteG);
				break;
				case 6:
					columnas = 7;
					titulo = new Paragraph(encabeza+"\nRastreo por Importe desde \""+dato1+"\" hasta \""+dato2+"\" ",whiteG);
				case 7:
					columnas = 7;
					titulo = new Paragraph(encabeza+"\nRastreo por identificador de ATS \""+dato1+"\" ",whiteG);
				break;
				default:
					System.out.println("No aplica esta seleccion.");
				break;
			}
			// Se definen las caracteristicas comunes de la tabla del PDF
			datatable = new PdfPTable(columnas);
			Image logo = Image.getInstance(rb.getValue("ta.logosantander"));
			PdfPCell cell = new PdfPCell(logo);
			cell.setColspan(2);
			cell.setBorderWidth(2);
			cell.setBorderColor(new BaseColor(0xFF, 0xFF, 0xFF));
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			datatable.addCell(cell);
			cell = new PdfPCell(titulo);
			cell.setColspan(columnas-2);
			cell.setBorderWidth(2);
			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorderColor(new BaseColor(0xFF, 0xFF, 0xFF));
			cell.setBackgroundColor(new BaseColor(0xF8, 0x00, 0x00));
			datatable.addCell(cell);
			datatable.setWidthPercentage(100); // porcentaje
			datatable.getDefaultCell().setPadding(3);
			datatable.getDefaultCell().setBorderWidth(2);
			datatable.getDefaultCell().setBorderColor(new BaseColor(0xFF, 0xFF, 0xFF));
			datatable.getDefaultCell().setBackgroundColor(new BaseColor(0x99, 0x99, 0x99));
			datatable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
			datatable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
			// Se selecciona la cantidad de campos por busqueda.
			switch(seleccion)
			{
				case 0:
					if("".equals(origen.trim())){
						int headerwidths0[] = { 20, 10, 50, 10, 10 };
						datatable.setWidths(headerwidths0);
					} else {
					int headerwidths0[] = { 20, 10, 55, 15 }; // porcentaje
					datatable.setWidths(headerwidths0);
					}

					datatable.addCell(new Paragraph("No. Operaci�n",white));
					datatable.addCell(new Paragraph("Hora",white));
					datatable.addCell(new Paragraph("Operaci�n",white));
					datatable.addCell(new Paragraph("Usuario",white));
					if("".equals(origen.trim())){
					datatable.addCell(new Paragraph("Origen",white));
				}
				break;
				case 1:
				case 5:
				case 6:
				case 7:
					int headerwidths1[] = { 20, 10, 20, 15, 10, 15, 10 }; // porcentaje
					datatable.setWidths(headerwidths1);
					datatable.addCell(new Paragraph("No. Operaci�n",white));
					datatable.addCell(new Paragraph("Hora",white));
					datatable.addCell(new Paragraph("Tipo Operaci�n",white));
					datatable.addCell(new Paragraph("Cuenta",white));
					datatable.addCell(new Paragraph("Folio",white));
					datatable.addCell(new Paragraph("Importe",white));
					datatable.addCell(new Paragraph("Estado",white));
				break;
				case 2:
					int headerwidths2[] = { 15, 7, 45, 10, 7, 8, 8 }; // porcentaje
					datatable.setWidths(headerwidths2);
					datatable.addCell(new Paragraph("No. Operaci�n",white));
					datatable.addCell(new Paragraph("Hora",white));
					datatable.addCell(new Paragraph("Tipo Operaci�n",white));
					datatable.addCell(new Paragraph("Cuenta",white));
					datatable.addCell(new Paragraph("Folio",white));
					datatable.addCell(new Paragraph("Importe",white));
					datatable.addCell(new Paragraph("Estado",white));
				break;
				case 3:
					int headerwidths3[] = { 20, 10, 20, 15, 15, 20 }; // porcentaje
					datatable.setWidths(headerwidths3);
					datatable.addCell(new Paragraph("No. Operaci�n",white));
					datatable.addCell(new Paragraph("Hora",white));
					datatable.addCell(new Paragraph("Tipo Operaci�n",white));
					datatable.addCell(new Paragraph("Folio",white));
					datatable.addCell(new Paragraph("Importe",white));
					datatable.addCell(new Paragraph("Estado",white));
				break;
				case 4:
					int headerwidths4[] = { 20, 10, 70}; // porcentaje
					datatable.setWidths(headerwidths4);
					datatable.addCell(new Paragraph("No. Operaci�n",white));
					datatable.addCell(new Paragraph("Hora",white));
					datatable.addCell(new Paragraph("Operaci�n",white));
				break;
				default:
					System.out.println("No aplica esta seleccion.");
			}
			datatable.setHeaderRows(2); // this is the end of the table header
		}
		catch (Exception e)
		{
			System.out.println("Error in " + getClass().getName() + "\n" + e);
		}
		return datatable;
	}

/**
 * El m�todo esContingente(String, String) revisa si los datos se encuentran
 * en la secion de contingente en base al punto de venta y fecha.
 *
 * @param fecha  Fecha de consulta
 * @param pventa Punto de venta
 * @return  ArrayList valor de respuesta de la consulta
 */
	@Override
	public ArrayList esContingente(String fecha, String pventa)
	{
		System.out.println("M�todo esContingente");
		ArrayList resultado = new ArrayList();
		StringBuffer lsb_query = new StringBuffer();
		PreparedStatement query = null;
		ResultSet rs = null;
		lsb_query = lsb_query.delete(0, lsb_query.length());
		lsb_query.append("SELECT SC.CVE_PTOVTA ");
		lsb_query.append("FROM TA_SUC_CONTINGENTE SC ");
		lsb_query.append("WHERE SC.CVE_PTOVTA = '").append(pventa).append("' ");
		lsb_query.append("AND SC.FECHA_DIA = TO_DATE('").append(fecha).append("','DD/MM/YYYY') ");
		lsb_query.append("UNION ALL ");
		lsb_query.append("SELECT SCH.CVE_PTOVTA ");
		lsb_query.append("FROM TA_SUC_CONST_HIST SCH ");
		lsb_query.append("WHERE SCH.CVE_PTOVTA = '").append(pventa).append("' ");
		lsb_query.append("AND SCH.FECHA_DIA = TO_DATE('").append(fecha).append("','DD/MM/YYYY') ");
		System.out.println("lsb_query: "+lsb_query.toString());
		try
		{
			if(con == null || con.isClosed())
				con = conexion.getConexion();
			query = con.prepareStatement(lsb_query.toString());
			rs=query.executeQuery();
			while(rs.next())
			{
				resultado.add("En esa fecha el Punto de Venta se encuentra en estado de contingencia.");
			}
		}
		catch (SQLException e1)
		{
			System.out.println("Error in " + getClass().getName() + "\n" + e1);
		}
		finally
		{
			try
			{
				if(rs!=null) rs.close();
				if(query!=null) query.close();
				//27/06/2008 Se realiza el cierre de conexion a l final de cada metodo
				if(con !=null) con.close();
				conexion.CloseConnection();

			}
			catch(SQLException e2)
			{
				System.out.println("Error in " + getClass().getName() + "\n" + e2);
			}
		}
		return resultado;
	}

/**
 * El m�todo allSucursal() obtiene la consulta de las Sucursales.
 *
 * @return  ArrayList valor de respuesta de la consulta
 */
	@Override
	public ArrayList allSucursal()
	{
		System.out.println("M�todo allSucursal");
		DAOConexionImpl conexion= new DAOConexionImpl();
		Connection con;
		ArrayList resultado = new ArrayList();
		StringBuffer lsb_query = new StringBuffer();
		PreparedStatement query = null;
		ResultSet rs = null;
		lsb_query = lsb_query.delete(0, lsb_query.length());
		lsb_query.append("SELECT CVE_PTOVTA, TRAB_SABADO, DESCRIPCION ");
		lsb_query.append("FROM TA_SUCURSAL ");
		lsb_query.append("ORDER BY CVE_PTOVTA ");
		System.out.println("lsb_query: "+lsb_query.toString());
		try
		{
			con = conexion.getConexion();
			query = con.prepareStatement(lsb_query.toString());
			rs=query.executeQuery();
			while(rs.next())
			{
				DTOTiras valores = new DTOTiras();
				valores.setPVenta(rs.getString("CVE_PTOVTA"));
				valores.setTraSabado(rs.getString("TRAB_SABADO"));
				valores.setDescripcion(rs.getString(DESCRIPCION));
				resultado.add(valores);
			}
			if(con!=null) con.close();
		}
		catch (SQLException e1)
		{
			System.out.println("Error in TiraClase.class\n" + e1);
		}
		finally
		{
			try
			{
				if(rs!=null) rs.close();
				if(query!=null) query.close();
				conexion.CloseConnection();
			}
			catch(SQLException e2)
			{
				System.out.println("Error in TiraClase.class\n" + e2);
			}
		}
		return resultado;
	}

/**
 * El m�todo descCustodio(String) obtiene el nombre del custodio.
 *
 * @param cve_custodio  Clave del custodio
 * @return  String nombre del custodio
 */
	@Override
	public String descCustodio(String cve_custodio)
	{
		System.out.println("M�todo descCustodio");
		DAOConexionImpl conexion= new DAOConexionImpl();
		Connection con;
		String resultado = new String();
		StringBuffer lsb_query = new StringBuffer();
		PreparedStatement query = null;
		ResultSet rs = null;
		lsb_query = lsb_query.delete(0, lsb_query.length());
		lsb_query.append("SELECT NOMBRE ");
		lsb_query.append("FROM TA_CUSTODIO ");
		lsb_query.append("WHERE CVE_CUSTODIO = '"+cve_custodio+"' ");
		System.out.println("lsb_query: "+lsb_query.toString());
		try
		{
			con = conexion.getConexion();
			query = con.prepareStatement(lsb_query.toString());
			rs=query.executeQuery();
			if(rs.next())
			{
				resultado  = rs.getString("NOMBRE");
			}
			if(!(resultado!=null && resultado.length()>4))
				resultado = cve_custodio;
			if(con!=null) con.close();

		}
		catch (SQLException e1)
		{
			System.out.println("Error in TiraClase.class\n" + e1);
		}
		finally
		{
			try
			{
				if(rs!=null) rs.close();
				if(query!=null) query.close();
				conexion.CloseConnection();
			}
			catch(SQLException e2)
			{
				System.out.println("Error in TiraClase.class\n" + e2);
			}
		}
		return resultado.trim();
	}

/**
 * El m�todo consultaCheques(String, String, String) obtiene la consulta de la informacion de
 * cheques en base al punto de venta y fecha.
 *
 * @param fecha  Fecha de consulta
 * @param pventa Punto de venta
 * @return  ArrayList valor de respuesta de la consulta
 */
	@Override
	public ArrayList consultaCheques(String fecha, String pventa)
	{
		System.out.println("M�todo consultaCheques");
		ArrayList resultado = new ArrayList();
		StringBuffer lsb_query = new StringBuffer();
		PreparedStatement query = null;
		ResultSet rs = null;
		lsb_query = lsb_query.delete(0, lsb_query.length());
		lsb_query.append("SELECT REFERENCIA, CUENTA, FOLIO, BANCO, PLAZA, IMPORTE, VERIFICA_FIRMA ");
		lsb_query.append("FROM TA_CHEQUES ");
		lsb_query.append("WHERE CVE_PTOVTA = '").append(pventa).append("' ");
		lsb_query.append("AND TO_CHAR(FECHA_DIA,'DD/MM/YYYY') = '").append(fecha).append("' ");
		lsb_query.append("ORDER BY REFERENCIA ");
		System.out.println("lsb_query: "+lsb_query.toString());
		try
		{
			if(con == null || con.isClosed())
				con = conexion.getConexion();
			query = con.prepareStatement(lsb_query.toString());
			rs=query.executeQuery();
			while(rs.next())
			{
				DTOTiras valores = new DTOTiras();
				valores.setNoOperacion(rs.getString("REFERENCIA"));
				valores.setCuenta(rs.getString("CUENTA"));
				valores.setFolio(rs.getString("FOLIO"));
				valores.setBanco(rs.getString("BANCO"));
				valores.setPlaza(rs.getString("PLAZA"));
				valores.setImporte(rs.getString("IMPORTE"));
				valores.setVerifica_firma(rs.getString("VERIFICA_FIRMA"));
				resultado.add(valores);
			}
		}
		catch (SQLException e1)
		{
			System.out.println("Error in " + getClass().getName() + "\n" + e1);
		}
		finally
		{
			try
			{
				if(rs!=null) rs.close();
				if(query!=null) query.close();
				//27/06/2008 Se realiza el cierre de conexion a l final de cada metodo
				if(con !=null) con.close();
				conexion.CloseConnection();
			}
			catch(SQLException e2)
			{
				System.out.println("Error in " + getClass().getName() + "\n" + e2);
			}
		}
		return resultado;
	}

/**
 * El m�todo registrosCheques(ArrayList, String) obtiene la consulta de la informacion de
 * cheques en base a la referencia.
 *
 * @param cheques  registros de cheques
 * @param referencia No. de operacion
 * @return  ArrayList valor de respuesta de la consulta
 */
	@Override
	public ArrayList registrosCheques(ArrayList cheques, String referencia)
	{
		//RSP 04-Abril-2008 System.out.println("M�todo registroCheques");
		ArrayList resultado = new ArrayList();
		if(cheques != null)
		{
			int tam1 = cheques.size();
			for(int i=0;i<tam1;i++)
			{
				DTOTiras tv = (DTOTiras) cheques.get(i);
				if(referencia.equals(tv.getNoOperacion()))
				{
					resultado.add(tv);
				}
			}
		}
		return resultado;
	}

/**
 * El m�todo registrosInv_Amon(ArrayList, String) obtiene la consulta de la informacion de
 * inv_amon en base a la referencia.
 *
 * @param inv_amon  registros de cheques
 * @param referencia No. de operacion
 * @return  ArrayList valor de respuesta de la consulta
 */
	@Override
	public ArrayList registrosInv_Amon(ArrayList inv_amon, String referencia)
	{
		//RSP 04-Abril-2008 System.out.println("M�todo registroInv_Amon");
		ArrayList resultado = new ArrayList();
		if(inv_amon != null)
		{
			int tam1 = inv_amon.size();
			for(int i=0;i<tam1;i++)
			{
				DTOTiras tv = (DTOTiras) inv_amon.get(i);
				if(referencia.equals(tv.getNoOperacion()))
				{
					resultado.add(tv);
				}
			}
		}
		return resultado;
	}

/**
 * El m�todo consultaInv_Amon(String, String, String) obtiene la consulta de la informacion de
 * inv_amon en base al punto de venta y fecha.
 *
 * @param fecha  Fecha de consulta
 * @param pventa Punto de venta
 * @return  ArrayList valor de respuesta de la consulta
 */
	@Override
	public ArrayList consultaInv_Amon(String fecha, String pventa)
	{
		//RSP 04-Abril-2008 System.out.println("M�todo consultaInv_Amon");
		ArrayList resultado = new ArrayList();
		StringBuffer lsb_query = new StringBuffer();
		PreparedStatement query = null;
		ResultSet rs = null;
		lsb_query = lsb_query.delete(0, lsb_query.length());
		lsb_query.append("SELECT REFERENCIA, TIPO, DENOMINACION, CANTIDAD ");
		lsb_query.append("FROM TA_INV_AMON ");
		lsb_query.append("WHERE CVE_PTOVTA = '").append(pventa).append("' ");
		lsb_query.append("AND TO_CHAR(FECHA_DIA,'DD/MM/YYYY') = '").append(fecha).append("' ");
		lsb_query.append("ORDER BY REFERENCIA ");
		System.out.println("lsb_query: "+lsb_query.toString());
		try
		{
			if(con == null || con.isClosed())
				con = conexion.getConexion();
			query = con.prepareStatement(lsb_query.toString());
			rs=query.executeQuery();
			while(rs.next())
			{
				DTOTiras valores = new DTOTiras();
				valores.setNoOperacion(rs.getString("REFERENCIA"));
				valores.setTipo(rs.getString("TIPO"));
				valores.setDenominacion(rs.getString("DENOMINACION"));
				valores.setCantidad(rs.getString("CANTIDAD"));
				resultado.add(valores);
			}
		}
		catch (SQLException e1)
		{
			System.out.println("Error in " + getClass().getName() + "\n" + e1);
		}
		finally
		{
			try
			{
				if(rs!=null) rs.close();
				if(query!=null) query.close();
				//27/06/2008 Se realiza el cierre de conexion a l final de cada metodo
				if(con !=null) con.close();
				conexion.CloseConnection();
			}
			catch(SQLException e2)
			{
				System.out.println("Error in " + getClass().getName() + "\n" + e2);
			}
		}
		return resultado;
	}

/**
 * El m�todo estadoDescripcion(String) genera la descrici�n del estado.
 *
 * @param clave Clave del Estado
 * @return String Descripci�n del Estado
 */
	@Override
	public String estadoDescripcion(String clave)
	{
		String descripcion;
		if(clave==null)
			clave = " ";
		switch(clave.charAt(0))
		{
			case 'R': descripcion = "RECIBIDA"; 					break;
			case 'X': descripcion = "RECHAZADA"; 					break;
			case 'A': descripcion = "ACEPTADA"; 					break;
			case 'T': descripcion = "NO AFECTO TOTALES"; 			break;
			case 'C': descripcion = "ANULADA"; 						break;
			case 'P': descripcion = "PENDIENTE"; 					break;
			case 'E': descripcion = "ENVIADA"; 						break;
			case 'F': descripcion = "REINGRESO"; 					break;
			case 'M': descripcion = "RECHAZADA MQ"; 				break;
			case 'Q': descripcion = "CONCLUIDA MQ"; 				break;
			case 'O': descripcion = "SOLICITUD DE ORDEN"; 			break;
			case 'L': descripcion = "SOLICITUD DE LIQUIDACION 1"; 	break;
			case 'S': descripcion = "PEDIMENTO LIBERADO"; 			break;
			case 'D': descripcion = "PENDIENTE DE CONFIRMAR"; 		break;
			default: descripcion = String.valueOf(clave); break;
		}
		return descripcion;
	}

/**
 * El m�todo operacion(DTOTiras, String, String) obtiene la consulta de la Tira
 * en base al punto de venta y fecha.
 *
 * @param val valores del registro
 * @return  String valor del detalle operaci�n
 */
	@Override
	public String operacion(DTOTiras val)
	{
		//RSP 04-Abril-2008 System.out.println("Metodo operacion");
		DecimalFormat dec = new DecimalFormat("$ ###,###,###,###,##0.00");
		StringBuffer desglose = new StringBuffer();
		int opera_local = 0;
		String inventario = "";
		String moneda = (val.getCveDivisa()==null || val.getCveDivisa().equals(""))?"MN":val.getCveDivisa().trim();
		String loc_estado = estadoDescripcion(val.getEstado());
		if(!val.getCodError().equals("VENT0000") && !val.getCodError().equals("TUBO0000") && !val.getCodError().equals("AVTC0000"))
			loc_estado = loc_estado + "/ERROR";
		double arrastre =  Double.parseDouble(val.getArrastre());
		String list = "";
		if(arrastre == 0)
			  list = "";
		else
			if(arrastre < 0)
				list = " \"C " + dec.format(arrastre * (-1)) + "\" ";
		 	else
				list = " \"P " + dec.format(arrastre) + "\" ";
		String observacion = val.getObservacion()!=null?val.getObservacion().trim():"";// Observaciones
		int indice = 0;
		indice = observacion.indexOf("@ALTRAD@");
		if(indice > 0)
			observacion = observacion.substring(0,indice);// Se elimina trama ALTRAD de observaciones
		indice = observacion.indexOf("OP_MODO_LOCAL");
		if(indice > 0)
			opera_local = 1;
		if(Integer.parseInt(val.getEnLinea()) == 0 && opera_local == 0) // Es reenvio
			if(val.getTipoOperacion().substring(0,1).equals("1"))
				if(!(val.getTipoOperacion().trim().equals("1INAM") || val.getTipoOperacion().trim().equals("1EGAM")))
					if(val.getEstado().equals("A"))
						loc_estado = loc_estado + "/PENDIENTE";
					else
						if(val.getEstado().equals("Y"))
							loc_estado = "REENVIO CANCELADO";
						else
							if(val.getEstado().equals("R"))
								loc_estado = "RECIBIDA";
							else
								if (val.getCodError().trim().equals("VENT0000"))
									loc_estado = loc_estado + "/ACEPTADA";
								else
									loc_estado = loc_estado + "/RECHAZADA";
		// Busca los registros de la tabla Cheques.
		ArrayList cheques = val.getCheques();
		String cuenta = "";
		int tam1 = cheques.size();
		for(int i=0;i<tam1;i++)
		{
			DTOTiras valCheque = (DTOTiras)cheques.get(i);
			cuenta = valCheque.getCuenta()!=null?valCheque.getCuenta():"";
			if( val.getTipoOperacion().trim().substring(1,5).equals("DEVO"))
			{
				int pos = 0;
				pos = observacion.indexOf(";",pos)+1;
				desglose.append("  Negocio " + parsea(observacion,pos,0));
				pos = observacion.indexOf(";",pos)+1;
				String proteccion = parsea(observacion,pos,0);
				pos = observacion.indexOf(";",pos)+1;
				int loc_inicio = proteccion.trim().indexOf(":");
				if(loc_inicio > 0)
					proteccion = proteccion.trim().substring(loc_inicio+1,proteccion.length());
				desglose.append("  Protecc " + proteccion);
			}
			  //RSP 04-Abril-2008
			 if(val.getTipoOperacion().substring(0,2).equals("1S") || val.getTipoOperacion().substring(0,2).equals("0S")){
				//RSP 04-Abril-2008 se valida la long. de la cuenta >0
				if(cuenta.length()>0){
				 desglose.append("Svo " + desc_Servicios(cuenta.substring(6,10)) + "   Folio " + valCheque.getFolio() + "  ");
				}
				else{//RSP 04-Abril-2008 se pasan ceros a la desc. del servicio, debido a que no trae datos
				desglose.append("Svo " + desc_Servicios("0000") + "   Folio " + valCheque.getFolio() + "  ");
				}
			  }
	          //RSP
			  else{
				//RSP 04-Abril-2008 System.out.println("Tipo de OPERACION:"+val.getTipoOperacion().substring(1,4));
				if(val.getTipoOperacion().substring(1,4).equals("CAC") || val.getTipoOperacion().substring(1,4).equals("ABC"))
				{
					desglose.append("Oper " + contable_Desc(cuenta));
					if(cuenta.trim().equals("0561"))
					{
						desglose.append(" Afiliacion " + parsea(observacion,0,0));
					}
					DecimalFormat fc = new DecimalFormat("####,####,####,####");
					if(cuenta.trim().equals("0463") || cuenta.trim().equals("0464"))
					{
						int pos = 0;
						desglose.append(" Aut. " + parsea(observacion,pos,0));
						pos = observacion.indexOf(";",pos)+1;
						desglose.append(" N�m Tarjeta " + fc.format(Long.parseLong(parsea(observacion,pos,0))).replace(',','-'));
						pos = observacion.indexOf(";",pos)+1;
					}
				}
				else
				{
					if(cuenta.trim().length() == 16){
						StringBuffer sb = new StringBuffer("");
						for (int c = 0; c < cuenta.length(); c += 4) {
							if (c != 0) {
								sb.append('-');
							}
							sb.append(cuenta.substring(c, c + 4));
						}
						cuenta = sb.toString();
					}
					desglose.append("Cuenta " + cuenta + "   Folio " + valCheque.getFolio() + "  ");
				}
			}
		}
		String loc_descripcion = "";
		if(val.getTipoOperacion().equals("SDOCT"))
		{

			loc_descripcion = "Consulta Saldo " + " \"" + loc_estado + "\" ";
			inventario = rastreo_DSaldos(observacion,cuenta);
			desglose = desglose.delete(0, desglose.length());
		}
		else
		{
			if(val.getTipoOperacion().equals("MNTAJ"))
			{
				val.setTipoOperacion(parsea(observacion,0,0));
			}
			if(opera_local == 1)
				loc_estado = loc_estado + "/LOCAL (" + val.getCveCustodio2() + ")";
			loc_descripcion =  val.getOperacion().trim() + " \"" + loc_estado.trim() + "\" ";
			//int loc_num = loc_descripcion.indexOf(".");
			//if(loc_num > 0)
			loc_descripcion = loc_descripcion + " \"Imp " + dec.format(Double.parseDouble(val.getImporte())) + moneda + "\" ";
		}
		loc_descripcion = loc_descripcion + list;
		// Busca los registros de la tabla Inv_Amon.
		ArrayList inv_Amon = val.getInv_amon();
		for(int i=0;i<inv_Amon.size();i++)
		{
			DTOTiras valInvAmon = (DTOTiras)inv_Amon.get(i);
			if(valInvAmon.getTipo().equals("9"))
				inventario = inventario + "[Fraccionaria]";
			else
			{
				inventario = inventario + "[" + valInvAmon.getDenominacion() + "]";
				DecimalFormat df = new DecimalFormat("#####,##0");
				inventario = inventario + "[" + df.format(Double.parseDouble(valInvAmon.getCantidad())) + "]";
			}
			double loc_Cantidad = Double.parseDouble(valInvAmon.getDenominacion()) * Double.parseDouble(valInvAmon.getCantidad());
			inventario = inventario + " " + dec.format(loc_Cantidad) + "   ";
		}
		inventario = inventario + desglose.toString();
		String claves = "1PVTC,1PVGR,1PREC,1PRES,1PTRF,1PREM,1CBIL,1CDVC,1VBIL,1VDVC,1VGIR,1VTRA,1TREM,1CTCE,1CTCC,1VTCE,1VTCC,1RECO,1RESC,1DLMN,1MNDL,1DLSN,1NDLS";
		int num = claves.indexOf(val.getTipoOperacion());
		if(!val.getCodError().equals("VENT0000") && !val.getCodError().equals("TUBO0000"))
		{
			if(num > 0)
				inventario = inventario + " ERROR ... " + descrOprCam(observacion, val.getTipoOperacion());
			else
				if(val.getTipoOperacion().equals("1CDOT"))
					inventario = inventario + " " + observacion;
				else
					inventario = inventario + " ERROR ... " + observacion;
		}
		if(num > 0)
			inventario = inventario + " " + descrOprCam(observacion, val.getTipoOperacion());
		inventario = loc_descripcion + inventario;
		return inventario;
	}

/**
 * El m�todo desc_Servicios(String) genera la descrici�n del servicio.
 *
 * @param servicio Clave del Servicio
 * @return String Descripci�n del Estado
 */
	@Override
	public String desc_Servicios(String servicio)
	{
		String servicios[][] = {{".","."},
								{"0003","TELEFONOS DE MEXICO"},
								{"0004","COMPA�IA DE LUZ Y FUERZA DEL CENTRO"},
								{"0005","AVON COSMETICS"},
								{"0006","RADIO MOVIL DIPSA"},
								{"0007","FOVISSSTE"},
								{"0008","COMISION FEDERAL ELECTRICIDAD"},
								{"0009","UNIVERSIDAD A DEL EDO DE MEXICO"},
								{"0010","GAS"},
								{"0011","COLEGIO EDUCACION Y CULTURA"},
								{"0012","COLEGIO EL NI�O, A.C."},
								{"0013","INSTITUTO MAZATLAN"},
								{"0014","INSTITUTO CULTURAL OCCIDENTE"},
								{"0015","SOMERCSA"},
								{"0016","INSTITUTO BILINGUE JEAN PIAGET"},
								{"0017","COLEGIO SINALOA"},
								{"0018","C.B.T.I.S."},
								{"0019","PRODES RECINTO MEMORIAL"},
								{"0020","BAJA CELULAR"},
								{"0021","S.A.A.C. (INFONAVIT)"},
								{"0022","INSTITUTO JUAREZ"},
								{"0023","INSTITUTO MONTES DE OCA"},
								{"0024","ESCUELA 18 DE MARZO"},
								{"0025","1972 A�O DE JUAREZ"},
								{"0026","TELECABLE"},
								{"0027","UNIVERSIDAD MICHOACANA"},
								{"0028","COMERCIAL CAMELINAS"},
								{"0029","C.E.M."},
								{"0031","CEPSA"},
								{"0032","CLUB DEP. TECOMENSE"},
								{"0041","PREDIAL"},
								{"0042","AGUA"},
								{"0043","IMPUESTOS LOCALES"},
								{"0045","MULTAS"},
								{"0046","SAR"},
								{"0072","MVS / DIRECTV"},
								{"0074","EKONOM"},
								{"0090","IMPUESTOS FEDERALES"}};
	   String descripcion = "";
	   for(int i = 0;i<38;i++)
	   {
	   		if(servicios[i][0].equals(""))
	   		{
			  descripcion = "";
			  i = 44;
	   		}
	   		else
	   			if(servicio!=null && servicio.equals(servicios[i][0]))
				{
					descripcion = servicios[i][1];
					i = 44;
				}
	   }
	   return descripcion;
	}

/**
 * El m�todo contable_Desc(String) genera la descrici�n del contable.
 *
 * @param servicio Clave del contable
 * @return String Descripci�n del contable
 */
	@Override
	public String contable_Desc(String servicio)
	{
		String svo_contable[][] = {{"05","CANCELA ACREEDORES DIF OPERACION","07","*","CACO","E"},
								{"06","CANCELA DEUDORES DIF OPERACION","07","*","ABCO","I"},
								{"07","DEPOSITO EFECTIVO BANXICO","03","00","CACO","E"},
								{"07a","RETIRO EFECTIVO BANXICO","03","00","ABCO","I"},
								{"08","DIPOSICIONES CAJERO AUTOMATICO","04","00","CACO","E"},
								{"09","CUENTAS POR PAGAR","05","00","ABCO","I"},
								{"10","COMISION CARTA LIBERACION CRD HIPOTECARIO","07","00","ABCO","I"},
								{"11","COMISION SERVICIOS ESPECIALES","07","00","ABCO","I"},
								{"12","IVA SERVICIOS ESPECIALES","07","00","ABCO","I"},
								{"13","AVALUOS","05","00","ABCO","I"},
								{"13a","REEMBOLSO AVALUOS","05","00","CACO","E"},
								{"14","INVESTIGACION Y/O ESTUDIO SOCIOECONOMICO","07","00","ABCO","I"},
								{"15","PAGARES Y LETRAS DE CAMBIO ENTRADA","06","00","CACO","X"},
								{"15a","PAGARES Y LETRAS DE CAMBIO SALIDA","06","00","ABCO","X"},
								{"16","SOBRE LACRADO Y/O CERRADO ENTRADA","06","00","CACO","X"},
								{"16a","SOBRE LACRADO Y/O CERRADO SALIDA","06","00","ABCO","X"},
								{"17","CARTAS DE GARANTIA ENTRADA","06","00","CACO","X"},
								{"17a","CARTAS DE GARANTIA SALIDA","06","00","ABCO","X"},
								{"18","TESTIMONIOS Y ESCRITURAS ENTRADA","06","00","CACO","X"},
								{"18a","TESTIMONIOS Y ESCRITURAS SALIDA","06","00","ABCO","X"},
								{"19","BOLSAS LACRADAS, CERRADAS ENTRADA","06","00","CACO","X"},
								{"19a","BOLSAS LACRADAS, CERRADAS SALIDA","06","00","ABCO","X"},
								{"20","CONVENIOS ENTRADA","06","00","CACO","X"},
								{"20a","CONVENIOS SALIDA","06","00","ABCO","X"},
								{"21","DEPOSITO GARANTIA CAJA SEGURIDAD","07","00","ABCO","I"},
								{"21a","ANULA DEPOSITO GARANTIA CAJA SEGURIDAD","07","00","CACO","E"},
								{"22","IVA CAJA SEGURIDAD","07","00","ABCO","I"},
								{"23","COMISION ALQUILER CAJA SEGURIDAD","07","00","ABCO","I"},
								{"24","CUENTA DE ORDEN CAJA SEGURIDAD","06","00","CACO","X"},
								{"24a","CANCELACION CUENTA DE ORDEN CAJA SEGURIDAD","06","00","ABCO","X"},
								{"27","FALTANTES DE CAJA","07","*","CACO","E"},
								{"28","SOBRANTES DE CAJA","07","*","ABCO","I"},
								{"31","TRASPASOS CAJERO AUTOMATICO","07","*","CACO","E"},
								{"31a","CANCELACION DE EXCEDENTE CAJERO AUTOMATICO","07","*","ABCO","I"},
								{"32","DOTACION CHEQUES VIAJERO","06","*","CACO","X"},
								{"60","DEVOLUCION DE IMPUESTOS TESOFE","04","00","CACO","E"},
								{"61","PENSIONADOS","05","00","CACO","E"},
								{"62","CANCELACION DE ADEUDOS VENCIDOS","07","*","ABCO","I"},
								{"63","CARGA MONEDERO","04","00","ABCO","I"},
								{"64","DESCARGA MONEDERO","04","00","CACO","E"}};
	   String descripcion = "";
	   for(int i = 0;i<39;i++)
	   {
			if(svo_contable[i][0].equals(""))
			{
			  descripcion = "";
			  i = 40;
			}
			else
			{
				String codigo = svo_contable[i][2] + svo_contable[i][0];
				if(servicio!=null && servicio.equals(codigo))
				{
					descripcion = svo_contable[i][1];
					i = 40;
				}
			}
	   }
	   return descripcion;
	}

/**
 * El m�todo rastreo_DSaldos(String, String) genera la descrici�n de la operacion.
 *
 * @param trama campo observaciones
 * @param cuenta N�mero de cuenta
 * @return String Descripci�n de la operacion
 */
	@Override
	public String rastreo_DSaldos(String trama, String cuenta)
	{
		StringBuffer resultado = new StringBuffer();
		String loc_producto = cuenta.length()>0?cuenta.trim().substring(0,2):"";
		resultado.append("Num Cuenta " + cuenta.trim() + "  ");
		DecimalFormat df = new DecimalFormat("###,###,###,###,##0.00 ");
		int pos = 0;
		resultado.append("[Nombre " + parsea(trama,pos,0) + "] ");
		pos = trama.indexOf(";",pos)+1;
		if(parsea(trama,pos,1).length()>0)
			resultado.append("[Disponible " + df.format(Double.parseDouble(parsea(trama,pos,1))) + "] ");
		else
			resultado.append("[Disponible ] ");
		pos = trama.indexOf(";",pos)+1;
		if(parsea(trama,pos,1).length()>0)
			resultado.append("[SBC " + df.format(Double.parseDouble(parsea(trama,pos,1))) + "] ");
		else
			resultado.append("[SBC ] ");
		pos = trama.indexOf(";",pos)+1;
		String loc_total = parsea(trama,pos,1);
		pos = trama.indexOf(";",pos)+1;
		double Loc_TotalCp = Double.parseDouble(loc_total.length()>0?loc_total:"0");
		if(parsea(trama,pos,1).length()>0)
			resultado.append("[Bloqueo " + df.format(Double.parseDouble(parsea(trama,pos,1))) + "] ");
		else
			resultado.append("[Bloqueo ] ");
		pos = trama.indexOf(";",pos)+1;
		resultado.append("[TOTAL " + df.format(Loc_TotalCp) + "] ");
		if(loc_producto.equals("60") || loc_producto.equals("65"))
		{
			pos = trama.indexOf(";",pos)+1;
			double vista = 0.00d;
			if(parsea(trama,pos,1).length()>0)
				vista = Double.parseDouble(parsea(trama,pos,1));
			pos = trama.indexOf(";",pos)+1;
			Loc_TotalCp = Loc_TotalCp + vista;
			resultado.append("[VISTA " + df.format(vista) + "] ");
			double plazo = 0.00d;
			if(parsea(trama,pos,1).length()>0)
				plazo = Double.parseDouble(parsea(trama,pos,1));
			Loc_TotalCp = Loc_TotalCp + plazo;
			resultado.append("[PLAZO " + df.format(plazo) + "] ");
			resultado.append("[UDI's " + df.format(0.00d) + "] ");
			resultado.append("[Fondos " + df.format(0.00d) + "] ");
			resultado.append("[Total Portafolio " + df.format(Loc_TotalCp) + "] ");
		}
		return resultado.toString();
	}

/**
 * El m�todo descrOprCam(String, String) genera la descrici�n de la operacion.
 *
 * @param trama campo observaciones
 * @param oper Operacion
 * @return String Descripci�n de la operacion
 */
	@Override
	public String descrOprCam(String trama, String oper)
	{
		int pos;
		StringBuffer resp = new StringBuffer();
		String impo = "";
		pos = 0;
		String claves = "1PVTC,1PVGR,1PREC,1PRES,1PTRF,1PREM";
		int num = claves.indexOf(oper);
		if(num >= 0)
		{
			pos = trama.indexOf(";",pos)+1;
			resp.append("Titular[" + parsea(trama,pos,0) + "] ");
			pos = trama.indexOf(";",pos)+1;
			resp.append("Cuenta Abo.[" + parsea(trama,pos,0) + "] ");
			pos = trama.indexOf(";",pos)+1;
			resp.append("Beneficiario[" + parsea(trama,pos,0) + "] ");
			pos = trama.indexOf(";",pos)+1;
			resp.append("Divisa[" + parsea(trama,pos,0) + "] ");
			pos = trama.indexOf(";",pos)+1;
			resp.append("Importe[" + parsea(trama,pos,0) + "] ");
			pos = trama.indexOf(";",pos)+1;
			pos = trama.indexOf(";",pos)+1;
			resp.append("Banco[" + parsea(trama,pos,0) + "] ");
			pos = trama.indexOf(";",pos)+1;
			pos = trama.indexOf(";",pos)+1;
			pos = trama.indexOf(";",pos)+1;
			pos = trama.indexOf(";",pos)+1;
			resp.append("ABA[" + parsea(trama,pos,0) + "] ");
			pos = trama.indexOf(";",pos)+1;
			pos = trama.indexOf(";",pos)+1;
			pos = trama.indexOf(";",pos)+1;
			pos = trama.indexOf(";",pos)+1;
			if(oper.equals("1PVGR") || oper.equals("1PREC") || oper.equals("1PRES"))
				resp.append("Folio Giro[" + parsea(trama,pos,0) + "] ");
			else
				if(oper.equals("1PREM"))
				{
					pos = trama.indexOf(";",pos)+1;
					resp.append("Folio Documento[" + parsea(trama,pos,0) + "] ");
				}
		}
		else
		{
			claves = "1CBIL,1CDVC,1VBIL,1VDVC,1VGIR,1VTRA,1TREM,1CTCE,1CTCC,1VTCE,1VTCC,1RECO,1RESC,1DLMN,1MNDL,1DLSN,1NDLS";
			num = claves.indexOf(oper);
			if(num >= 0)
			{
				pos = trama.indexOf(";",pos)+1;
				pos = trama.indexOf(";",pos)+1;
				resp.append("BENEF.[" + parsea(trama,pos,0) + "] ");
				pos = trama.indexOf(";",pos)+1;
				pos = trama.indexOf(";",pos)+1;
				pos = trama.indexOf(";",pos)+1;
				resp.append("LUGAR[" + parsea(trama,pos,0) + "] ");
				pos = trama.indexOf(";",pos)+1;
				resp.append("FOLIO[" + parsea(trama,pos,0) + "] ");
				pos = trama.indexOf(";",pos)+1;
				resp.append("TIPO CAMBIO[" + parsea(trama,pos,0) + "] ");
				pos = trama.indexOf(";",pos)+1;
				impo = parsea(trama,pos,0);
				pos = trama.indexOf(";",pos)+1;
				DecimalFormat df = new DecimalFormat("###,###,###.00");
				if(parsea(trama,pos,1).length() > 0)
					impo = df.format(Double.parseDouble(parsea(trama,pos,0))) + " " + impo;
				resp.append("IMPORTE[" + impo + "] ");
				pos = trama.indexOf(";",pos)+1;
				pos = trama.indexOf(";",pos)+1;
				resp.append("N.ORDEN[" + parsea(trama,pos,0) + "] ");
				pos = trama.indexOf(";",pos)+1;
				pos = trama.indexOf(";",pos)+1;
				pos = trama.indexOf(";",pos)+1;
				pos = trama.indexOf(";",pos)+1;
				resp.append("LIQ.1[" + parsea(trama,pos,0) + "] ");
				pos = trama.indexOf(";",pos)+1;
				resp.append("LIQ.2[" + parsea(trama,pos,0) + "] ");
			}
			else
			{
				resp.append("");
			}
		}
		return resp.toString();
	}

/**
 * El m�todo parsea(String, int, int) valida la cadena a pintar.
 *
 * @param trama campo observaciones
 * @param pos Posicion en la cadena
 * @param num validador si es numerico o string
 * @return String Cadena validada
 */
	@Override
	public String parsea(String trama, int pos, int num)
	{
		int val = 0;
		String retorno = "";
		if(pos >= 0)
		{
			val = trama.indexOf(";",pos);
			if(val>0)
			{
				if(trama.substring(pos,trama.indexOf(";",pos)).length() > 0)
				{
					retorno = trama.substring(pos,trama.indexOf(";",pos));
					if(num == 1)
						try
						{
							Double.parseDouble(retorno);
						}
						catch(Exception e)
						{
							retorno = "";
						}
				}
				else
					retorno = "";
			}
			else
			{
				if(trama.substring(pos,trama.length()).length()>0)
				{
					retorno = trama.substring(pos,trama.length());
					if(num == 1)
						try
						{
							Double.parseDouble(retorno);
						}
						catch(Exception e)
						{
							retorno = "";
						}
				}
				else
					retorno = "";
			}
		}
		return retorno.trim();
	}

	//** SE AGREGA METODO para obtener la clave del custodio

	/**
	 * El metodo consultaClaveCustodio(String) obtiene la clave del custodio
	 * en base al punto de venta.
	 *
	 * @param pventa Punto de venta
	 * @return  ArrayList valor de respuesta de la consulta
	 */
	@Override
		public ArrayList consultaClaveCustodio(String pventa)
		{
			//System.out.println("M�todo consultaClaveCustodio");
			ArrayList resultado = new ArrayList();
			StringBuffer lsb_query = new StringBuffer();
			PreparedStatement query = null;
			ResultSet rs = null;
			lsb_query = lsb_query.delete(0, lsb_query.length());
			lsb_query.append("SELECT TC.CVE_CUSTODIO ");
			lsb_query.append("FROM TA_CUSTODIO TC, TA_SUCURSAL TS ");
			lsb_query.append("WHERE CVE_PTOVTA = '").append(pventa).append("' ");
			lsb_query.append("AND TC.CVE_CUSTODIO LIKE '").append(pventa).append("' ||'%' ");
			System.out.println("lsb_query: "+lsb_query.toString());
			try
			{
				if(con == null || con.isClosed()) {
					con = conexion.getConexion();
			}
				query = con.prepareStatement(lsb_query.toString());
				rs=query.executeQuery();
				while(rs.next()) {
					DTOTiras valores = new DTOTiras();
					valores.setClaveCustodio(rs.getString(CVE_CUSTODIO));
					resultado.add(valores);
				}
			}
			catch (SQLException e1) {
				System.out.println("Error in " + getClass().getName() + "\n" + e1);
			}

			finally {
			try {
				if(rs!=null) {
				rs.close();
				}
				if(query!=null) {
					query.close();
				}
					//Se realiza el cierre de conexion al final de cada metodo
					if(con !=null) {
						con.close();
					}
					conexion.CloseConnection();
				}
				catch(SQLException e2) {
					System.out.println("Error in " + getClass().getName() + "\n" + e2);
				}
			}
			return resultado;
		} // TERMINA METODO para obtener la clave del custodio


}
