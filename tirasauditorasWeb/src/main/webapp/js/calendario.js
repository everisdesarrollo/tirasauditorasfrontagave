// Title: Calendario
// Description: Pantalla de calendario
// Version: 3.0
// Date: 06-02-2006 (mm-dd-yyyy)
// Author: David Aguilar G. <spiderdag@hotmail.com>

// months as they appear in the calendar's title
var ARR_MONTHS = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
		"Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
// week day titles as they appear on the calendar
var ARR_WEEKDAYS = ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"];
// day week starts from (normally 0-Mo or 1-Su)
var NUM_WEEKSTART = 1;
// path to the directory where calendar images are stored. trailing slash req.
var STR_ICONPATH = '../images/';
// if two digit year input dates after this year considered 20 century.
var NUM_CENTYEAR = 30;

var calendars = [];
var RE_NUM = /^\d+$/;

function calendario(obj_target,actual_date) {

	// assing methods
	this.gen_date = cal_gen_date3;
	this.gen_time = cal_gen_time3;
	this.prs_date = cal_prs_date3;
	this.prs_time = cal_prs_time3;
	this.prs_datetime = cal_prs_datetime3;
	this.popup    = cal_popup3;

	// validate input parameters
	if (!obj_target)
		return alert("Error calling the calendar: no target control specified");
	if (obj_target.value == null)
		return alert("Error calling the calendar: parameter specified is not valid tardet control");
	this.target = obj_target;
	if (!actual_date)
		return alert("Error calling the calendar: no target control specified");
	if (actual_date.value == null)
		return alert("Error calling the calendar: parameter specified is not valid tardet control");
	this.a_date = actual_date;

	// register in global collections
	this.id = calendars.length;
	calendars[this.id] = this;
}

function cal_popup3 (str_datetime) {
	if (!str_datetime)
		str_datetime = this.target.value;
	this.dt_current = this.prs_datetime(str_datetime);
	if (!this.dt_current) return;

	// get same date in the previous year
	var dt_prev_year = new Date(this.dt_current);
	dt_prev_year.setFullYear(dt_prev_year.getFullYear() - 1);
	if (dt_prev_year.getDate() != this.dt_current.getDate())
		dt_prev_year.setDate(0);

	// get same date in the next year
	var dt_next_year = new Date(this.dt_current);
	dt_next_year.setFullYear(dt_next_year.getFullYear() + 1);
	if (dt_next_year.getDate() != this.dt_current.getDate())
		dt_next_year.setDate(0);

	// get same date in the previous month
	var dt_prev_month = new Date(this.dt_current);
	dt_prev_month.setMonth(dt_prev_month.getMonth() - 1);
	if (dt_prev_month.getDate() != this.dt_current.getDate())
		dt_prev_month.setDate(0);

	// get same date in the next month
	var dt_next_month = new Date(this.dt_current);
	dt_next_month.setMonth(dt_next_month.getMonth() + 1);
	if (dt_next_month.getDate() != this.dt_current.getDate())
		dt_next_month.setDate(0);

	// get first day to display in the grid for current month
	var dt_firstday = new Date(this.dt_current);
	dt_firstday.setDate(1);
	dt_firstday.setDate(1 - (7 + dt_firstday.getDay() - NUM_WEEKSTART) % 7);

	// html generation (feel free to tune it for your particular application)
	// print calendar header
	var obj_calwindow = window.open("", "Calendario",
		"width=225,height=222,status=no,resizable=no,top=200,left=200");
	obj_calwindow.opener = window;
	if (esFechaValida){
		var dt_actual_day = new Date(this.a_date.value);
		var dt_current_day = new Date(dt_firstday);
		var str_buffer = new String (
		'<html>'+
		'<head>'+
			'<title>Elija la Fecha</title>'+
			'<script language="JavaScript">'+
			'var bandera = 0;'+
			'function set_datetime(n_datetime, b_close) {'+
				'var obj_calendar = window.opener.calendars['+this.id+'];'+
				'var dt_datetime = obj_calendar.prs_time(document.cal.time.value, new Date(n_datetime));'+
				'if (!dt_datetime) return;'+
				'obj_calendar.target.value = obj_calendar.gen_date(dt_datetime);'+
				'if (b_close) window.close();'+
				'else obj_calendar.popup(dt_datetime.valueOf());'+
			'}'+
			'function cierraVentana() {'+
				'var obj_calendar = window.opener.calendars['+this.id+'];'+
				'if(bandera==0)'+
				'obj_calendar.target.value = "";'+
			'}'+
			'function mensaje(ban) {'+
				'if(ban=="0")'+
				'document.cal.text.value = "Domingo no se labora.";'+
				'else '+
				'document.cal.text.value = "D�a fuera de rango permitido.";'+
				'if(ban=="99")'+
				'document.cal.text.value = "";'+
			'}'+
			'</script>'+
		'</head>'+
		'<body bgcolor="White" onunload="cierraVentana()">'+
		'<table class="clsOTable" cellspacing="0" border="0" width="100%">'+
		'<tr><td bgcolor="#9d0000">'+
		'<table cellspacing="1" cellpadding="3" border="0" width="100%">'+
		'<tr>' );
		str_buffer += 	'<td bgcolor="#9d0000"><a href="javascript:set_datetime('+dt_prev_year.valueOf()+')"><img src="'+STR_ICONPATH+'prev_year.gif" width="16" height="16" border="0" alt="anio anterior"></a></td>';
		str_buffer += 	'<td bgcolor="#9d0000"><a href="javascript:set_datetime('+dt_prev_month.valueOf()+')"><img src="'+STR_ICONPATH+'prev.gif" width="16" height="16" border="0" alt="mes anterior"></a></td>';
		str_buffer += 	'<td bgcolor="#9d0000" colspan="3" align="center"><font color="white" face="tahoma, verdana" style="font-size: 10px">'+ARR_MONTHS[this.dt_current.getMonth()]+' '+this.dt_current.getFullYear()+'</font></td>';
		str_buffer += 	'<td bgcolor="#9d0000" align="right"><a href="javascript:set_datetime('+dt_next_month.valueOf()+')"><img src="'+STR_ICONPATH+'next.gif" width="16" height="16" border="0" alt="mes proximo"></a></td>';
		str_buffer += 	'<td bgcolor="#9d0000" align="right"><a href="javascript:set_datetime('+dt_next_year.valueOf()+')"><img src="'+STR_ICONPATH+'next_year.gif" width="16" height="16" border="0" alt="anio proximo"></a></td>';
		str_buffer += '</tr><tr>';

		// print weekdays titles
		for (var n=0; n<7; n++)
			str_buffer += '<td bgcolor="#ff0000" align="center"><font color="white" face="arial, tahoma, verdana" size="2">'+ARR_WEEKDAYS[(NUM_WEEKSTART+n)%7]+'</font></td>';
		str_buffer += '</tr>';

		// print calendar table
		var dt_current_day = new Date(dt_firstday);
		while (dt_current_day.getMonth() == this.dt_current.getMonth() ||
			dt_current_day.getMonth() == dt_firstday.getMonth()) {
			// print row heder
			str_buffer += '<tr>';
			for (var n_current_wday=0; n_current_wday<7; n_current_wday++) {
					if (dt_current_day.getDate() == this.dt_current.getDate() &&
						dt_current_day.getMonth() == this.dt_current.getMonth())
						// print current date
						str_buffer += '<td bgcolor="#DBEAF5" align="center">';
					else if (dt_current_day.getDay() == 0 || dt_current_day.getDay() == 6)
						// weekend days
						str_buffer += '<td bgcolor="#ffd5d5" align="center">';
					else
						// print working days of current month
						str_buffer += '<td bgcolor="white" align="center">';

					if (dt_current_day.getMonth() == this.dt_current.getMonth()){
						// print days of current month
						//alert(dt_current_day.getMonth()+'-'+dt_current_day.getDate()+' >= '+dt_actual_day.getMonth()+'_'+dt_actual_day.getDate());
						if(dt_current_day.getDay() == 0 ||
						   (dt_current_day.getDate() >= dt_actual_day.getDate() && dt_current_day.getMonth() == dt_actual_day.getMonth() && dt_current_day.getYear() == dt_actual_day.getYear()) ||
						   dt_current_day.getMonth() > dt_actual_day.getMonth() && dt_current_day.getYear() == dt_actual_day.getYear() ||
						   dt_current_day.getYear() > dt_actual_day.getYear()){
							str_buffer += '<a href="javascript:mensaje('+dt_current_day.getDay()+');" style="color: black; font-size: 12px; font-family: arial, tahoma, verdana;" onmouseover="javascript:mensaje('+dt_current_day.getDay()+');">';
						}else{
							str_buffer += '<a href="javascript:bandera=1;set_datetime('+dt_current_day.valueOf()+', true)" style="color: black; font-size: 12px; font-family: arial, tahoma, verdana;" onmouseover="javascript:mensaje(99);">';
						}
					}else{
						// print days of other months
						if(dt_current_day.getDay() == 0 ||
						    (dt_current_day.getMonth() > dt_actual_day.getMonth() && dt_current_day.getYear() == dt_actual_day.getYear()) ||
						    (dt_current_day.getMonth() == dt_actual_day.getMonth() && dt_current_day.getDate() >= dt_actual_day.getDate() && dt_current_day.getYear() == dt_actual_day.getYear()) ||
						    dt_current_day.getYear() > dt_actual_day.getYear()){
							str_buffer += '<a href="javascript:mensaje('+dt_current_day.getDay()+');" style="color: gray; font-size: 12px; font-family: arial, tahoma, verdana;" onmouseover="javascript:mensaje('+dt_current_day.getDay()+');">';
						}else{
							str_buffer += '<a href="javascript:bandera=1;set_datetime('+dt_current_day.valueOf()+', true)" style="color: gray; font-size: 12px; font-family: arial, tahoma, verdana;" onmouseover="javascript:mensaje(99);">';
						}
					}
					str_buffer += dt_current_day.getDate()+'</a></td>';
					dt_current_day.setDate(dt_current_day.getDate()+1);
			}
			// print row footer
			str_buffer += '</tr>';
		}
		// print calendar footer
		str_buffer +=
			'<form action="javascript:set_datetime('+this.dt_current.valueOf()+', true)" name="cal"><tr><td colspan="7" bgcolor="#ff0000"><font color="White" face="Arial, tahoma, verdana" size="2"><input type="hidden" name="time" value="'+this.gen_time(this.dt_current)+'" size="8" maxlength="8"></font></td></tr>' +
			'</table></tr></td>'+
			'<tr><td><INPUT type="text" name="text" size="28" readonly></td></tr></form>'+
			'</table></body></html>';

		obj_calwindow.document.write(str_buffer);
		obj_calwindow.document.close();
		obj_calwindow.focus();
	}
}

// date generating function
function cal_gen_date3 (dt_datetime) {
	return (
		/*new String (
			(dt_datetime.getDate() < 10 ? '0' : '') + dt_datetime.getDate() + "/"
			+ (dt_datetime.getMonth() < 9 ? '0' : '') + (dt_datetime.getMonth() + 1) + "/"
			+ dt_datetime.getFullYear() + ' '
			+ (dt_datetime.getHours() < 10 ? '0' : '') + dt_datetime.getHours() + ":"
			+ (dt_datetime.getMinutes() < 10 ? '0' : '') + (dt_datetime.getMinutes()) + ":"
			+ (dt_datetime.getSeconds() < 10 ? '0' : '') + (dt_datetime.getSeconds())
		)*/
		new String (
			(dt_datetime.getDate() < 10 ? '0' : '') + dt_datetime.getDate() + "/"
			+ (dt_datetime.getMonth() < 9 ? '0' : '') + (dt_datetime.getMonth() + 1) + "/"
			+ dt_datetime.getFullYear()
		)
	);
}
// time generating function
function cal_gen_time3 (dt_datetime) {
	return (
		new String (
			(dt_datetime.getHours() < 10 ? '0' : '') + dt_datetime.getHours() + ":"
			+ (dt_datetime.getMinutes() < 10 ? '0' : '') + (dt_datetime.getMinutes()) + ":"
			+ (dt_datetime.getSeconds() < 10 ? '0' : '') + (dt_datetime.getSeconds())
		)
	);
}


// datetime parsing function
function cal_prs_datetime3 (str_datetime) {
	// if no parameter specified return current timestamp
	if (!str_datetime)
		return (new Date());

	// if positive integer treat as milliseconds from epoch
	if (RE_NUM.exec(str_datetime))
		return new Date(str_datetime);
	str_datetime += ' ';
	var arr_datetime = str_datetime.split(' ');
	//return this.prs_time(arr_datetime[1], this.prs_date(arr_datetime[0]));
	return this.prs_time(null, this.prs_date(arr_datetime[0]));
}

// date parsing function
function cal_prs_date3 (str_date) {

	var arr_date = str_date.split('/');

	if (arr_date.length != 3) return alert ("Formato inv�lido: '" + str_date + "'.\nEl Formato aceptado es dd/mm/yyyy.");
	if (!arr_date[0]) return alert ("Formato inv�lido: '" + str_date + "'.\nNo existe el d�a del mes.");
	if (!RE_NUM.exec(arr_date[0])) return alert ("D�a del mes inv�lido: '" + arr_date[0] + "'.\nNo es n�mero v�lido.");
	if (!arr_date[1]) return alert ("Formato inv�lido: '" + str_date + "'.\nNo month value can be found.");
	if (!RE_NUM.exec(arr_date[1])) return alert ("Mes inv�lido: '" + arr_date[1] + "'.\nNo es n�mero v�lido.");
	if (!arr_date[2]) return alert ("Formato inv�lido: '" + str_date + "'.\nNo year value can be found.");
	if (!RE_NUM.exec(arr_date[2])) return alert ("A�o inv�lido: '" + arr_date[2] + "'.\nNo es n�mero v�lido.");

	var dt_date = new Date();

	if (arr_date[1] < 1 || arr_date[1] > 12)
        return alert ("Mes inv�lido: '" + arr_date[1] + "'.\nEl rango es 01-12.");
    dt_date.setMonth(arr_date[1]-1);

	if (arr_date[2] < 100) arr_date[2] = Number(arr_date[2]) + (arr_date[2] < NUM_CENTYEAR ? 2000 : 1900);
	dt_date.setFullYear(arr_date[2]);

	var dt_numdays = new Date(arr_date[2], arr_date[1], 0);
	dt_date.setDate(arr_date[0]);
    if (dt_date.getMonth() != (arr_date[1]-1)){ // Aqui puse el parche
        dt_date.setMonth(dt_date.getMonth()-1);
    }
	if (arr_date[0] > dt_numdays.getDate()){
        return alert ("Dia del mes invalido: '" + arr_date[0] + "'.\nEl rango es 01-"+dt_numdays.getDate()+".");
    }
	return (dt_date)
}

// time parsing function
function cal_prs_time3 (str_time, dt_date) {

	if (!dt_date) return;

	/*var arr_time = str_time.split(':');

	if (!arr_time[0]) dt_date.setHours(0);
	else if (RE_NUM.exec(arr_time[0]))
		if (arr_time[0] < 24) dt_date.setHours(arr_time[0]);
		else return alert ("Invalid hours value: '" + arr_time[0] + "'.\nAllowed range is 00-23.");
	else return alert ("Invalid hours value: '" + arr_time[0] + "'.\nAllowed values are unsigned integers.");

	if (!arr_time[1]) dt_date.setMinutes(0);
	else if (RE_NUM.exec(arr_time[1]))
		if (arr_time[1] < 60) dt_date.setMinutes(arr_time[1]);
		else return alert ("Invalid minutes value: '" + arr_time[1] + "'.\nAllowed range is 00-59.");
	else return alert ("Invalid minutes value: '" + arr_time[1] + "'.\nAllowed values are unsigned integers.");

	if (!arr_time[2]) dt_date.setSeconds(0);
	else if (RE_NUM.exec(arr_time[2]))
		if (arr_time[2] < 60) dt_date.setSeconds(arr_time[2]);
		else return alert ("Invalid seconds value: '" + arr_time[2] + "'.\nAllowed range is 00-59.");
	else return alert ("Invalid seconds value: '" + arr_time[2] + "'.\nAllowed values are unsigned integers.");

	dt_date.setMilliseconds(0);*/
	return dt_date;
}

function esFechaValida(str_date){
	var RE_NUM = /^\d+$/;
	var arr_date = str_date.split('/');

	if (arr_date.length != 3) return alert ("Formato invalido: '" + str_date + "'.\nEl Formato aceptado es dd/mm/yyyy.");
	if (!arr_date[0]) return alert ("Formato invalido: '" + str_date + "'.\nNo existe el dia del mes.");
	if (!RE_NUM.exec(arr_date[0])) return alert ("Dia del mes invalido: '" + arr_date[0] + "'.\nNo es numero valido.");
	if (!arr_date[1]) return alert ("Formato inv�lido: '" + str_date + "'.\nNo month value can be found.");
	if (!RE_NUM.exec(arr_date[1])) return alert ("Mes invalido: '" + arr_date[1] + "'.\nNo es numero valido.");
	if (!arr_date[2]) return alert ("Formato invalido: '" + str_date + "'.\nNo year value can be found.");
	if (!RE_NUM.exec(arr_date[2])) return alert ("A�o invalido: '" + arr_date[2] + "'.\nNo es numero valido.");

	var dt_date = new Date();

	if (arr_date[1] < 1 || arr_date[1] > 12) return alert ("Mes invalido: '" + arr_date[1] + "'.\nEl rango es 01-12.");
	dt_date.setMonth(arr_date[1]-1);

	if (arr_date[2] < 100) arr_date[2] = Number(arr_date[2]) + (arr_date[2] < NUM_CENTYEAR ? 2000 : 1900);
	dt_date.setFullYear(arr_date[2]);

	var dt_numdays = new Date(arr_date[2], arr_date[1], 0);
	dt_date.setDate(arr_date[0]);
	if (dt_date.getMonth() != (arr_date[1]-1))
        return alert ("Dia del mes "+dt_date.getMonth()+" invalido: "+arr_date[1]-1+" '" + arr_date[0] + "'.\nEl rango es 01-"+dt_numdays.getDate()+".");

	return true;
}

function esFechaValida(str_date){
	var RE_NUM = /^\d+$/;
	var arr_date = str_date.split('/');

	if (arr_date.length != 3) return alert ("Formato inv�lido: '" + str_date + "'.\nEl Formato aceptado es dd/mm/yyyy.");
	if (!arr_date[0]) return alert ("Formato inv�lido: '" + str_date + "'.\nNo existe el d�a del mes.");
	if (!RE_NUM.exec(arr_date[0])) return alert ("D�a del mes inv�lido: '" + arr_date[0] + "'.\nNo es n�mero v�lido.");
	if (!arr_date[1]) return alert ("Formato inv�lido: '" + str_date + "'.\nNo month value can be found.");
	if (!RE_NUM.exec(arr_date[1])) return alert ("Mes inv�lido: '" + arr_date[1] + "'.\nNo es n�mero v�lido.");
	if (!arr_date[2]) return alert ("Formato inv�lido: '" + str_date + "'.\nNo year value can be found.");
	if (!RE_NUM.exec(arr_date[2])) return alert ("A�o inv�lido: '" + arr_date[2] + "'.\nNo es n�mero v�lido.");

	var dt_date = new Date();

	if (arr_date[1] < 1 || arr_date[1] > 12) return alert ("Mes inv�lido: '" + arr_date[1] + "'.\nEl rango es 01-12.");
	dt_date.setMonth(arr_date[1]-1);

	if (arr_date[2] < 100) arr_date[2] = Number(arr_date[2]) + (arr_date[2] < NUM_CENTYEAR ? 2000 : 1900);
	dt_date.setFullYear(arr_date[2]);

	var dt_numdays = new Date(arr_date[2], arr_date[1], 0);
	dt_date.setDate(arr_date[0]);
	if (dt_date.getMonth() != (arr_date[1]-1)) return alert ("D�a del mes inv�lido: '" + arr_date[0] + "'.\nEl rango es 01-"+dt_numdays.getDate()+".");

	return (dt_date);
}

function traerFechaActual () {
	var dt_datetime = new Date ();
	return (
		new String (
			(dt_datetime.getDate() < 10 ? '0' : '') + dt_datetime.getDate() + "/"
			+ (dt_datetime.getMonth() < 9 ? '0' : '') + (dt_datetime.getMonth() + 1) + "/"
			+ dt_datetime.getFullYear()
		)
	);
}