package mx.isban.tirasauditoras.tiraauditora.dao.impl;

import java.sql.Connection;
import java.sql.SQLException;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import mx.isban.tirasauditoras.tiraauditora.dao.DAOConexion;
import mx.isban.agave.commons.architech.Architech;

@Stateless
@Local(DAOConexion.class)
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOConexionImpl extends Architech implements DAOConexion {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8215040765725747905L;
	public DAOConexionImpl()
    {
      conexion1 = null;
    }
	
	@Override
	public Connection getConexion() throws SQLException {
		if(conexion1 == null || conexion1.isClosed())
	    {
	      try
	        {
	          InitialContext ic = new InitialContext();
	          DataSource ds = (DataSource)ic.lookup("java:comp/env/db");
	          // 27/06/2008 ESC se quita la invocacion doble de conexion.
	          //ds.getConnection();
	          conexion1 = ds.getConnection();
	        }
	      catch(NamingException e)
	        {
	          e.printStackTrace();
	          System.out.println("Error en el contexto inicial o en el nombre del JNDI" + e);
	        }
	      catch(SQLException e)
	        {
	          e.printStackTrace();
	          System.out.println("Error al abrir la conexion" + e);
	        }
	    }
		return conexion1;
	}

	@Override
	public void CloseConnection() {
		 if(conexion1 !=null)
		    {
		      try
		        {
		          conexion1.close();
		        }
		        catch(SQLException e)
		        {
		          e.printStackTrace();
		          System.out.println("Error al cerra la conexion" + e);
		        }
		    }
		    conexion1 = null;
		
	}
	Connection conexion1;

}
